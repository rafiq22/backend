const Joi = require('joi');
const clc = require('cli-color');

module.exports = (err, req, res, next) => {
  if (Joi.isError(err)) {
    err = {
      statusCode: err.statusCode,
      message: err.message,
    }
    err.statusCode = err.statusCode || res.STATUS_CODE.BadRequest;
    if (err.message) err.message = err.message.replace(/"/g, "`")
  }

  console.group(clc.red.bold("ERROR 💥"));
  console.log(clc.red('Name: '), err.name);
  console.log(clc.red('Message: '), err.message);
  console.log(clc.red('StatusCode: '), err.statusCode);
  console.log(clc.red('Stack: '), err.stack?.slice(err.stack?.indexOf('\n')));
  console.groupEnd("ERROR 💥");

  console.log(err);
  if (!(err instanceof Object))
    err = {
      origin: err,
      stack: new Error().stack,
    };

  err.statusCode = err.statusCode || res.STATUS_CODE.InternalServerError;
  err.message = err.message || 'something went very wrong';

  if (process.env.NODE_ENV === 'development') {
    sendErrorDev(err, req, res);
  }
  else sendErrorProd(err, req, res);
};

const sendErrorDev = (err, req, res) => {
  const returnedJson = {
    success: false,
    error: {
      message: err.message,
      statusCode: err.statusCode,
      name: err.name,
      stack: err.stack,
    }
  };

  if (req.originalUrl.startsWith('/api')) {
    return res.status(err.statusCode).json(returnedJson);
  }

  // todo:uncomment those lines after completing error page in view folder
  // res.status(err.statusCode).render('error', returnedJson);
  res.send('error page should be here');
};

const sendErrorProd = (err, req, res) => {
  const returnedJson = {
    success: false,
    error: {
      message: err.message,
      statusCode: err.statusCode,
    }
  };

  if (req.originalUrl.startsWith('/api')) {
    return res.status(err.statusCode).json(returnedJson);
  }

  // todo:uncomment those lines after completing error page in view folder
  // res.status(err.statusCode).render('error', returnedJson);
  res.send('error page should be here');
};