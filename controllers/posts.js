const catchAsync = require('../utils/catchAsync');

const ClientError = require('../utils/ClientError');
const Post = require('../models/posts');
const Like = require('../models/like');

const _ = require('lodash');
const axios = require('axios');
const cloudinary = require('cloudinary').v2;

module.exports.create = catchAsync(async (req, res) => {
  let filePaths = [];
  for (const file of req.files)
    filePaths.push(file.path);

  const content = {
    text: req.body.text,
    files: filePaths,
  }
  await Post.createPost(req.params.profileId, content);

  res.json({
    success: true,
    results: {
      message: 'Post has been created successfully',
    }
  })
});

module.exports.getSomeUserPosts = catchAsync(async (req, res) => {
  let posts = await Post.getPosts(req.params.profileId);

  if (req.user.userName) {
    for (let post of posts) {
      const liked = await Like.isLiked(req.user.userName, post._id);
      post.isLiked = liked;
    }
  }

  res.json({
    posts: posts,
  });
});

module.exports.getMoreUserPosts = catchAsync(async (req, res) => {
  let posts = await Post.getMorePosts(req.params.profileId, req.params.lastPostId);

  if (req.user.userName) {
    for (let post of posts) {
      const liked = await Like.isLiked(req.user.userName, post._id);
      post.isLiked = liked;
    }
  }

  res.json({
    posts: posts,
  });
});

module.exports.get = catchAsync(async (req, res) => {
  const postId = req.params.postId;
  let post = await Post.getPost(postId);

  if (req.user.userName) {
    const liked = await Like.isLiked(req.user.userName, postId);
    post.isLiked = liked;
  }

  res.json({
    post: post,
  });
});

module.exports.update = catchAsync(async (req, res) => {
  // todo
  res.send('post update function');
});

module.exports.delete = catchAsync(async (req, res) => {
  // todo: delete files from cloudinary too
  const postId = req.params.postId;
  await Post.deletePost(postId);
  res.json({
    success: true,
    results: {
      message: 'the Post has been deleted successfully',
    }
  })
});

module.exports.getNumberOfLikes = getNumberOfLikes = catchAsync(async (req, res) => {
  const postId = req.params.postId;
  const numberOfLikes = await Like.getNumberOfLikes(postId, Post);
  res.json({
    success: true,
    results: {
      numberOfLikes: numberOfLikes,
    }
  })
});

module.exports.like = catchAsync(async (req, res, next) => {
  const postId = req.params.postId;
  const user = req.user.userName;
  await Like.like(user, postId, Post);


  const response = await axios({
    method: 'GET',
    url: `${req.protocol}://${req.get('host')}/api/v1/users/${req.params.profileId}/posts/${req.params.postId}/numberOfLikes`,
    headers: {
      'access-token': req.headers['access-token'],
    }
  });

  res.json({
    success: true,
    results: {
      numberOfLikes: response.data.results.numberOfLikes,
    }
  })
});

module.exports.unLike = catchAsync(async (req, res) => {
  const postId = req.params.postId;
  const user = req.user.userName;
  await Like.unLike(user, postId, Post);

  const response = await axios({
    method: 'GET',
    url: `${req.protocol}://${req.get('host')}/api/v1/users/${req.params.profileId}/posts/${req.params.postId}/numberOfLikes`,
    headers: {
      'access-token': req.headers['access-token'],
    }
  });

  res.json({
    success: true,
    results: {
      numberOfLikes: response.data.results.numberOfLikes,
    }
  })
});

module.exports.getUsersWhoLiked = catchAsync(async (req, res) => {
  const postId = req.params.postId;
  const likes = await Like.getUsersWhoLiked(postId);
  res.json({
    success: true,
    results: {
      likes: likes,
    }
  })
});

module.exports.isLiked = catchAsync(async (req, res, next) => {
  const user = req.user.userName;
  const postId = req.params.postId;

  const likedResult = await Like.isLiked(user, postId);

  res.send({
    success: true,
    results: {
      isLiked: likedResult
    },
  });
});

module.exports.incrementViews = catchAsync(async (req, res) => {
  const updatedPost = await Post.findOneAndUpdate(
    { _id: req.params.postId },
    { $inc: { numberOfViews: 1 } },
    { new: true });
  res.json({
    success: true,
    results: {
      post: updatedPost,
    }
  });
});

module.exports.share = catchAsync(async (req, res) => {
  const user = req.user.userName;
  const postId = req.params.postId;

  const content = {
    text: req.body.text,
  }
  await Post.sharePost(user, content, postId);

  res.json({
    success: true,
    results: {
      message: 'Post has been shared successfully',
    }
  })
});

module.exports.addMedia = catchAsync(async (req, res) => {
  // todo
  res.send('addMedia function');
});

module.exports.deleteMedia = catchAsync(async (req, res) => {
  // todo
  res.send('deleteMedia function');
});

module.exports.getSomeMedia = catchAsync(async (req, res) => {
  // todo
  res.send('getSomeMedia function');
});


module.exports.getOnlyImages = catchAsync(async (req, res) => {
  const data = await cloudinary.search
    .expression(`public_id=image/posts/${req.params.profileId}-*`)
    .sort_by('public_id', 'desc')
    .execute();


  const final_data = data.resources.map(el => _.pick(el, ['url']))

  res.json({
    success: true,
    results: {
      data: final_data,
    }
  });
});


module.exports.getOnlyVideos = catchAsync(async (req, res) => {
  const data = await cloudinary.search
    .expression(`public_id=video/posts/${req.params.profileId}-*`)
    .sort_by('public_id', 'desc')
    .execute();

  const final_data = data.resources.map(el => _.pick(el, ['url']))

  res.json({
    success: true,
    results: {
      data: final_data,
    }
  });
});