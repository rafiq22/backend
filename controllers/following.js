const catchAsync = require('../utils/catchAsync');
const Following = require('../models/followings');


module.exports.isFollowed = catchAsync(async (req, res, next) => {
    const user = req.user.userName;
    const profileId = req.params.profileId;
    const isFollowing = await Following.isFollowing(user, profileId);
    res.json({
        success: true,
        results: {
            isFollowing: isFollowing,
        }
    });
});

module.exports.follow = catchAsync(async (req, res, next) => {
    const user = req.user.userName;
    const profileId = req.params.profileId;
    const profileType = req.params.profileType;
    
    await Following.follow(user, profileId, profileType);

    res.json({
        success: true,
        results: {
           message:  `You are successfully following ${profileId}`, 
        }
    });
});


module.exports.unfollow = catchAsync(async (req, res, next) => {
    const user = req.user.userName;
    const profileId = req.params.profileId;
    const profileType = req.params.profileType;

    await Following.unFollow(user, profileId, profileType);

    res.json({
        success: true,
        results: {
            message: `You successfully unfollowed ${profileId}`,
        }
    });
});





// module.exports.isFollowed = catchAsync(async (req, res, next) => {
//   const user = req.user.userName;
//   const profileToFollow = req.params.profileId;

//   // check if you already followed the user 
//   const followingResult = await mongoose.model('followings').isFollowing(user, profileToFollow);

//   res.send({
//     success: true,
//     results: {
//       isFollowing: followingResult,
//     },
//   });
// });

// module.exports.followUser = catchAsync(async (req, res, next) => {
//     const user = req.user.userName;
//     const profileToFollow = req.params.profileId;
  
//     const isAlreadyFollowing = await mongoose.model('followings').isFollowing(user, profileToFollow);

//     if (isAlreadyFollowing)
//       return await next(
//         new ClientError(`You're already following ${profileToFollow}!`, res.STATUS_CODE.BadRequest)
//       );
  
//     const relation = await new mongoose.model('followings')({ source: user, sink: profileToFollow });
//     await relation.save();
  
//     res.send({
//       success: true,
//       results: {
//         message: `You are successfully following ${profileToFollow}`,
//       },
//     });
// });

// module.exports.unFollowUser = catchAsync(async (req, res, next) => {
//     const user = req.user.userName;
//     const profileToUnfollow = req.params.profileId;
  
//     // check if you already unFollowed the user before following 
//     const isAlreadyFollowing = await mongoose.model('followings').isFollowing(user, profileToUnfollow);

//     if (!isAlreadyFollowing)
//       return await next(
//         new ClientError(`You already do not follow ${profileToUnfollow}!`, res.STATUS_CODE.BadRequest)
//       );
  
  
//     // user unfollow profileToUnfollow
//     await mongoose.model('followings').deleteOne({ _id: `${user}>${profileToUnfollow}` });
  
//     res.send({
//       success: true,
//       results: {
//         message: `You successfully unfollowed ${profileToUnfollow}`,
//       },
//     });
//   });


// module.exports.incrementNumberOfFollowings = catchAsync(async (req, res, next)=>{
//     const user = req.user.userName;
//     const profileId =req.params.profileId;
//     const profileType = req.params.profileType;

//     mongoose.model('users').updateOne({_id: user }, {$inc: {numberOfFollowings: 1}});
//     mongoose.model(profileType).updateOne({_id: profileId}, {$inc: {numberOfFollowings: 1}});
// });


// module.exports.decrementNumberOfFollowings = catchAsync(async (req, res, next)=>{
//     const user = req.user.userName;
//     const profileId =req.params.profileId;
//     const profileType = req.params.profileType;

//     mongoose.model('users').updateOne({_id: user }, {$inc: {numberOfFollowings: -1}});
//     mongoose.model(profileType).updateOne({_id: profileId}, {$inc: {numberOfFollowings: -1}});
// });
  
  // module.exports.getFollowers = catchAsync(async (req, res, next) => {
  //   const user = req.params.profileId;
  
  //   const followers = await mongoose.model('followings').getFollowers(user);
  
  //   // if (!followers.length)
  //   //   return await next(
  //   //     new ClientError(`The user ${user} has no followers`, res.STATUS_CODE.BadRequest)
  //   //   );
  
  //   res.send({
  //     success: true,
  //     results: {
  //       followers: followers,
  //     },
  //   });
  // });
  
  // module.exports.getFollowings = catchAsync(async (req, res, next) => {
  //   const user = req.params.profileId;
  
  //   const followings = await mongoose.model('followings').getFollowings(user);
  
  //   // if (!followings.length)
  //   //   return await next(
  //   //     new ClientError(`The user ${user} has no followings`, res.STATUS_CODE.BadRequest)
  //   //   );
  
  //   res.send({
  //     success: true,
  //     results: {
  //       followings: followings,
  //     },
  //   });
  // });
  
  // module.exports.getNumberOfFollowers = catchAsync(async (req, res, next) => {
  //   const user = req.params.profileId;
  
  //   const numberOfFollowers = await mongoose.model('followings').getNumberOfFollowers(user);
  
  //   res.send({
  //     success: true,
  //     results: {
  //       numberOfFollowers: numberOfFollowers,
  //     },
  //   });
  // });
  
  // module.exports.getNumberOfFollowings = catchAsync(async (req, res, next) => {
  //   const user = req.params.profileId;
  
  //   const numberOfFollowings = await mongoose.model('followings').getNumberOfFollowings(user);
  
  //   res.send({
  //     success: true,
  //     results: {
  //       numberOfFollowings: numberOfFollowings,
  //     },
  //   });
  // });
  