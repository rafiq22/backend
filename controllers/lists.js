const catchAsync = require('../utils/catchAsync');
const List = require('../models/lists');
const mongoose = require('mongoose');

module.exports.getBlockList = catchAsync(async (req, res, next) => {
  const blockList = await List.findOne({ _id: req.user.userName }, { blockList: 1, _id: 0 });
  res.json({
    success: true,
    results: {
      blockList,
    }
  })
});


module.exports.getUserTravelMap = catchAsync(async (req, res) => {
  const doc = await List.findOne({ _id: req.params.profileId }, { _id: 0, travelMap: 1 });
  res.json({
    success: true,
    results: {
      travelMap: doc ? doc.travelMap : null,
    }
  });
});

module.exports.addMarkerToTravelMap = catchAsync(async (req, res) => {
  await List.updateOne({ _id: req.user.userName }, { $push: { travelMap: req.body } });
  // fixme: return _id of inserted travelMap element
  // with a properly method
  // because getting the last element in the array maybe wrong
  // while parallel insertion
  const doc = await List.findOne({
    _id: req.user.userName, travelMap: {
      $elemMatch: {
        ...req.body,
      }
    }
  }, { _id: 0, travelMap: 1 });

  res.json({
    success: true,
    results: {
      message: 'New marker has been added successfully!',
      marker: doc.travelMap[doc.travelMap.length - 1],
    }
  });
});

module.exports.deleteMarkerFromTravelMap = catchAsync(async (req, res) => {
  const markerId = mongoose.Types.ObjectId(req.body._id);
  await List.updateOne({ _id: req.user.userName }, { $pull: { travelMap: { _id: markerId } } });
  res.json({
    success: true,
    results: {
      message: 'It has been removed successfully!',
    }
  });
});