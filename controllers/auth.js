const bcrypt = require('bcryptjs');
const ClientError = require('../utils/ClientError.js');
const catchAsync = require('../utils/catchAsync');
const Tokens = require('../utils/Tokens');

const User = require('../models/users.js');
const List = require('../models/lists');
const Redis = require('redis');

const sendEmail = require('../utils/email.js');

module.exports.login = catchAsync(async (req, res, next) => {
  // not registered
  const emailOrUserName = req.body.emailOrUserName;
  const projectedDoc = { userName: 1, password: 1, confirmed: 1 };
  const user =
    (await User.findOne({ userName: emailOrUserName }, projectedDoc)) ||
    (await User.findOne({ email: emailOrUserName }, projectedDoc));

  if (!user)
    return await next(
      new ClientError(
        'email, userName or Password is not correct',
        res.STATUS_CODE.BadRequest
      )
    );

  // wrong password
  const validPassword = await bcrypt.compare(req.body.password, user.password);
  if (!validPassword)
    return await next(
      new ClientError(
        'email, userName or Password is not correct',
        res.STATUS_CODE.BadRequest
      )
    );

  if (!user.confirmed) {
    return await next(new ClientError('Please confirm your email via the link sent to your email address', res.STATUS_CODE.Forbidden));
  }

  // generate Tokens
  const tokenPayload = { userName: user.userName }
  const accessToken = Tokens.generateAccessToken(tokenPayload);
  const refreshToken = Tokens.generateRefreshToken(tokenPayload);

  await res
    .json({
      success: true,
      results: {
        message: 'Logged in successfully',
        user: { userName: user.userName },
        accessToken: accessToken, // ensure: should token be sent in body (for mobile) ?
        refreshToken: refreshToken, // ensure: should token be sent in body (for mobile) ?
      },
    });

  // store refreshToken in redis
  const redisClient = Redis.createClient();
  await redisClient.connect(); // ensure: does this line make response slow ?
  await redisClient.setEx(
    refreshToken,
    process.env.RT_EXPIRES_IN / 1000,
    user.userName
  );
});

module.exports.register = catchAsync(async (req, res, next) => {
  // first time to register
  let user = await User.findOne({ email: req.body.email });
  if (user)
    return next(
      new ClientError('User is already registered', res.STATUS_CODE.BadRequest)
    );

  // check user name taken or not
  user = await User.findOne({ userName: req.body.userName });
  if (user)
    return next(
      new ClientError('Username is already taken', res.STATUS_CODE.BadRequest)
    );

  // create the user to store in db
  // note: NO WORRIES about extra un-wanted property
  // as joi throw an error if extra properties are provided in req.body
  // In addition to mongoose will take it's properties and ignore the rest
  user = await new User(req.body);

  // store new user in the database
  await user.save();

  const list = await new List({ _id: user.userName });
  await list.save();

  res.status(res.STATUS_CODE.Created).send({
    success: true,
    results: {
      message: 'Account has been created successfully',
    },
  });
});

module.exports.forgotPassword = catchAsync(async (req, res, next) => {
  // check if user is registered
  const user =
    (await User.findOne({ userName: req.body.emailOrUserName })) ||
    (await User.findOne({ email: req.body.emailOrUserName }));

  // if user does not exist, do nothing
  if (!user) {
    return res.status(res.STATUS_CODE.NoContent).send();
  }

  // generate resetToken
  const resetToken = Tokens.generateResetToken({ userName: user.userName });
  console.log('resetToken: ', resetToken)
  // hash & update resetToken in DB
  const hashedResetToken = await bcrypt.hash(resetToken, 12);
  await User.updateOne(
    { userName: user.userName },
    {
      $set: { "resetToken": resetToken },  // todo hashedResetToken
    }
  );

  // send email to user
  const UrlToken = `${req.protocol}://${req.get(
    'host'
  )}/resetPassword/${resetToken}`;
  await sendEmail({
    email: user.email,
    username: user.userName,
    urlToken: UrlToken,
  });

  res.status(res.STATUS_CODE.NoContent).send();
});

module.exports.resetPassword = catchAsync(async (req, res, next) => {
  // get user who has the resetToken
  const user = await User.findOne({ userName: req.user.userName });

  if (!user) {
    // may be user delete his account this try resetPassword
    // before resetToken has been expired
    return await next(
      new ClientError("User doesn't exist", res.STATUS_CODE.InternalServerError)
    );
  }

  // todo: !(await bcrypt.compare(req.headers['reset-token'], user.resetToken))
  // todo: edit it after move resetToken to redis
  if (req.headers['reset-token'] !== user.resetToken) {
    return await next(
      new ClientError('Invalid token', res.STATUS_CODE.BadRequest)
    );
  }

  await User.updatePasswordByUserName(req.body.password, user.userName);

  res.send({
    success: true,
    results: {
      message: 'password has been updated successfully',
    },
  });
});

module.exports.getAccessToken = catchAsync(async (req, res, next) => {
  const oldRefreshToken = req.headers['refresh-token']
  const redisClient = Redis.createClient();
  await redisClient.connect(); // ensure: does this line make response slow ?
  const userName = await redisClient.get(oldRefreshToken);

  // old(given) refresh token not expired but not in db
  // reused refresh token
  if (!userName) {
    //todo
    ///// DANGER: using the same refresh token twice maybe (user got hacked)
    //// what the action ?
    console.log('user has been hacked, we must take an action');
    return next(
      new ClientError(
        'you are not allowed to get new token',
        res.STATUS_CODE.Forbidden
      )
    );
  }


  // we expect that userName that is stored in redis
  // is the same that is encrypted in the refreshToken
  const user = req.user;
  if (userName !== user.userName) {
    return next(
      new ClientError('Unexpected Error!', res.STATUS_CODE.InternalServerError)
    );
  }

  const tokenPayload = { userName: user.userName };
  const accessToken = Tokens.generateAccessToken(tokenPayload);
  const refreshToken = Tokens.generateRefreshToken(tokenPayload);

  res
    .send({
      success: true,
      results: {
        accessToken: accessToken,
        refreshToken: refreshToken,
      },
    });

  await redisClient.del(oldRefreshToken);
  const expirationTime = +process.env.RT_EXPIRES_IN / 1000; // in seconds
  await redisClient.setEx(refreshToken, expirationTime, userName);
});