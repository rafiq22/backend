const catchAsync = require('../utils/catchAsync');
const Fuse = require('fuse.js');
const { countryList } = require('../utils/constants');
const City = require('../models/city');
const User = require('../models/users')

const path = require('path');
const fs = require('fs');

const { PythonShell } = require('python-shell');
const { spawn } = require('child_process');

const ClientError = require('../utils/ClientError');
const axios = require('axios');


module.exports.getSuggestedUsers = catchAsync(async (req, res, next) => {
    if (req.query.type !== 'user') {
        return next();
    }
    req.query.q = req.query.q.trim();

    // todo: this is not the best search strategy
    const condition = [
        { userName: { $regex: `^${req.query.q}`, $options: 'i' } },
        { firstName: { $regex: `^${req.query.q}`, $options: 'i' } },
        { lastName: { $regex: `^${req.query.q}`, $options: 'i' } },
    ];

    const suggestions = await User.find({ $or: condition }).limit(5);

    res.json({
        success: true,
        results: {
            suggestions: suggestions,
        }
    });
});

module.exports.getSuggestedCities = catchAsync(async (req, res, next) => {
    // const suggestions = await City.aggregate([
    //     {
    //         $search: {
    //             compound: {
    //                 should: [
    //                     {
    //                         autocomplete: {
    //                             query: req.query.q,
    //                             path: 'firstName',
    //                             fuzzy: {
    //                                 maxEdits: 2,
    //                             },
    //                         },
    //                     },
    //                     {
    //                         autocomplete: {
    //                             query: req.query.q,
    //                             path: 'ascii_name',
    //                         },
    //                     },
    //                     {
    //                         autocomplete: {
    //                             query: req.query.q,
    //                             path: 'native_name',
    //                         },
    //                     },
    //                 ],
    //             },
    //         }
    //     },
    //     {
    //         $limit: 10,
    //     }
    // ]);

    req.query.q = req.query.q.trim();
    const conditions = [
        { firstName: { $regex: `^${req.query.q}`, $options: 'i' } },
        { ascii_name: { $regex: `^${req.query.q}`, $options: 'i' } },
    ];
    const suggestions = await City.find({ $or: conditions }).limit(5);

    res.json({
        success: true,
        results: {
            suggestions: suggestions,
        }
    });
});

module.exports.searchByImage = catchAsync(async (req, res, next) => {
    if (!req.files?.length) {
        return next(new ClientError('please provide an image', res.STATUS_CODE.BadRequest));
    }

    // const bestMatch = await new Promise((resolve, reject) => {
    //     PythonShell.run(
    //         path.normalize(`${__dirname}/../CBIR/search.py`),
    //         {
    //             args: [req.files[0].filename],
    //         },
    //         function (err, results) {
    //             // delete query image from desk
    //             fs.unlinkSync(`${__dirname}/../CBIR/queries/${req.files[0].filename}`);
    //             if (err) {
    //                 return reject(new ClientError('Sorry! something went wrong!', res.STATUS_CODE.BadRequest));
    //             }
    //             return resolve(results);
    //         })
    // });

    console.log('im here 1111111');
    const childPython = spawn('python3',
        [path.normalize(`${__dirname}/../CBIR/search.py`), req.files[0].filename]);

    console.log('im here 2222222');
    const bestMatch = await new Promise((resolve, reject) => {
        childPython.stdout.on('data', (data) => {
            fs.unlinkSync(`${__dirname}/../CBIR/queries/${req.files[0].filename}`);
            data = data.toString();
            data = data.split('\n');
            data.pop();
            return resolve(data);
        })
        childPython.stderr.on('data', (err) => {
            fs.unlinkSync(`${__dirname}/../CBIR/queries/${req.files[0].filename}`);
            return reject(err.toString());
        });
    });
    console.log('im here 333333');

    // 1) filter bestMatch to keep the top UNIQUE 5 places
    const st = new Set();
    const filteredBestMatch = [];
    for (const result of bestMatch) {
        if (!result) continue;
        const cityId = result.split('-')[0];
        if (!st.has(cityId)) {
            filteredBestMatch.push(result);
            st.add(cityId);
        }
        if (filteredBestMatch.length >= 5) break;
    }
    console.log('im here 444444');
    const finalResults = [];
    // 2) get those places from database
    for (const imageName of filteredBestMatch) {
        const cityId = imageName.split('-')[0];
        const city = (await axios({
            method: 'GET',
            url: `${req.protocol}://${req.get('host')}/api/v1/cities/${cityId}`,
            headers: {
                'access-token': req.headers['access-token'],
            }
        })).data?.results;

        if (!city) {
            return next(new ClientError("something went wrong while fetching best matched cities", res.STATUS_CODE.InternalServerError));
        }//
        else {
            city.matchedImage = `https://res.cloudinary.com/elaraby/image/upload/v1652967942/image/cbir/${imageName}`;
            finalResults.push(city);
        }
    }

    console.log('im here 55555555');
    // 3) send response
    res.json({
        success: true,
        results: {
            suggestions: finalResults,
        }
    });
});