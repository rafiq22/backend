const catchAsync = require('../utils/catchAsync');

const ClientError = require('../utils/ClientError');
const Trip = require('../models/trip');
const _ = require('lodash');
const axios = require('axios');
const cloudinary = require('cloudinary').v2;

module.exports.create = catchAsync(async (req, res) => {
  const obj = { ...req.body };
  obj.userName = req.user.userName;
  await Trip.createTrip(obj, req.files);

  res.json({
    success: true,
    results: {
      message: 'The Trip has been created successfully',
    }
  })
});

module.exports.getFilter = catchAsync(async (req, res) => {
  let trips = await Trip.findBestRated();

  if (req.body.filter === 'lowestPrice') {
    trips = await Trip.findLowestPrice();
  }
  else if (req.body.filter === 'highestPrice') {
    trips = await Trip.findHighestPrice();
  }

  res.json({
    success: true,
    trips: trips,
  })
});


module.exports.getById = catchAsync(async (req, res) => {
  const trip = await Trip.findById(req.params.tripId);
  res.json({
    success: true,
    trip: trip,
  })
});