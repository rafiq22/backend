const catchAsync = require('../utils/catchAsync');

const ClientError = require('../utils/ClientError');

const Like = require('../models/like');
const Comment = require('../models/comment');

const axios = require('axios');

module.exports.create = catchAsync(async (req, res) => {
  const user = req.user.userName;
  const text = req.body.text;
  const postId = req.params.postId;

  const commentId = await Comment.addComment(user, postId, text);

  res.json({
    success: true,
    results: {
      message: 'the comment has been created successfully',
      commentId: commentId,
    }
  })
});

module.exports.get = catchAsync(async (req, res) => {
  const postId = req.params.postId;
  let comments = await Comment.getComments(postId);

  if (req.user.userId) {
    for (let comment of comments) {
      comment.isLiked = await Like.isLiked(req.user.userName, comment._id);
    }
  }

  res.json({
    success: true,
    comments: comments,
  })
});

module.exports.getMoreComments = catchAsync(async (req, res) => {
  const postId = req.params.postId, lastCommentId = req.params.lastCommentId;
  let comments = await Comment.getMoreComments(postId, lastCommentId);

  if (req.user.userId) {
    for (let comment of comments) {
      comment.isLiked = await Like.isLiked(req.user.userName, comment._id);
    }
  }

  res.json({
    success: true,
    comments: comments,
  })
});

module.exports.update = catchAsync(async (req, res) => {
  const text = req.body.text;
  const commentId = req.params.commentId;

  await Comment.updatedComment(commentId, text);

  res.json({
    success: true,
    results: {
      message: 'the comment has been updated successfully',
    }
  })
});

module.exports.delete = catchAsync(async (req, res) => {
  const commentId = req.params.commentId;
  const postId = req.params.postId;

  await Comment.deleteComment(commentId, postId);

  res.json({
    success: true,
    results: {
      message: 'the comment has been deleted successfully',
    }
  })
});

module.exports.like = catchAsync(async (req, res, next) => {
  const commentId = req.params.commentId;
  const user = req.user.userName;
  await Like.like(user, commentId, Comment);


  const response = await axios({
    method: 'GET',
    url: `${req.protocol}://${req.get('host')}/api/v1/users/${req.params.profileId}/posts/${req.params.postId}/comments/${req.params.commentId}/like`,
    headers: {
      'access-token': req.headers['access-token'],
    }
  });

  res.json({
    success: true,
    results: {
      numberOfLikes: response.data.results.numberOfLikes,
    }
  })
});

module.exports.unLike = catchAsync(async (req, res, next) => {
  const commentId = req.params.commentId;
  const user = req.user.userName;
  await Like.unLike(user, commentId, Comment);

  const response = await axios({
    method: 'GET',
    url: `${req.protocol}://${req.get('host')}/api/v1/users/${req.params.profileId}/posts/${req.params.postId}/comments/${req.params.commentId}/like`,
    headers: {
      'access-token': req.headers['access-token'],
    }
  });


  res.json({
    success: true,
    results: {
      numberOfLikes: response.data.results.numberOfLikes,
    }
  })
});


module.exports.getNumberOfLikes = catchAsync(async (req, res) => {
  const commentId = req.params.commentId;
  const numberOfLikes = await Like.getNumberOfLikes(commentId, Comment);
  res.json({
    success: true,
    results: {
      numberOfLikes: numberOfLikes,
    }
  })
});

module.exports.getUsersWhoLiked = catchAsync(async (req, res) => {
  const commentId = req.params.commentId;
  const likes = await Like.getUsersWhoLiked(commentId);
  res.json({
    success: true,
    results: {
      likes: likes,
    }
  })
});