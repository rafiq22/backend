const catchAsync = require('../utils/catchAsync');
const ClientError = require('../utils/ClientError.js');

const User = require('../models/users.js');
const Following = require('../models/followings');
const List = require('../models/lists');

const { cld } = require('../utils/cloudinary');

module.exports.getUserProfile = catchAsync(async (req, res) => {
  const unprojectedFields = {
    _id: 0,
    email: 0,
    password: 0,
    resetToken: 0, // todo note it will be removed from mongodb and saved in redis
    confirmed: 0,
    __v: 0,
    posts: 0,
    newsFeed: 0,
  };

  const result = await User.findOne({ userName: req.params.profileId }, unprojectedFields).lean();
  result.isAdmin = result.userName === req.user.userName;
  // note: we don't have validation layer against block list
  // so if there is not an existing user, it will send response error.
  res.json({
    success: true,
    results: result,
  });
});

module.exports.deleteUser = catchAsync(async (req, res) => {
  // todo
  res.send('deleteUser function');
});

module.exports.getUserInfo = catchAsync(async (req, res) => {
  const projectedFields = {
    _id: 0,
    userName: 1,
    firstName: 1,
    lastName: 1,
    avatar: 1,
  };
  // const allowedFields = ['firstName', 'lastName', 'country', 'dateOfBirth', 'gender', 'liveIn']
  // for(const prop of allowedFields){
  //     projectedFields[prop] = 1;
  // }

  const result = await User.findOne({ userName: req.params.profileId }, projectedFields);

  res.json({
    success: true,
    results: result,
  });
});

module.exports.updateUserInfo = catchAsync(async (req, res, next) => {

  const user = req.user;
  await User.updateOne({ userName: user.userName }, { $set: req.body });
  res.json({
    success: true,
    results: {
      message: 'data has been updated successfully!'
    }
  });
});


module.exports.changePassword = catchAsync(async (req, res) => {
  await User.updatePasswordByUserName(req.body.password, req.user.userName);
  res.json({
    success: true,
    results: {
      message: 'password has been updated successfully',
    }
  })
});

module.exports.getUserAvatar = catchAsync(async (req, res) => {
  const doc = await User.findOne({ userName: req.params.profileId }, { avatar: 1, _id: 0 });

  res.json({
    success: true,
    results: doc,
  });

});

module.exports.updateUserAvatar = catchAsync(async (req, res, next) => {
  const userDoc = await User.findOne({ userName: req.user.userName }, { avatar: 1 });

  // fixme: doesn't delete
  if (userDoc.avatar)
    await cld.uploader.destroy(userDoc.avatar);

  // store new image path in mongodb
  const newData = {
    avatar: req.files[0].path,
  }
  await User.updateOne({ userName: req.user.userName }, { $set: newData });

  res.json({
    success: true,
    results: newData,
  })
});

module.exports.deleteUserAvatar = catchAsync(async (req, res) => {
  const userDoc = await User.findOne({ userName: req.user.userName }, { avatar: 1, _id: 0 });

  // fixme: doesn't delete
  if (userDoc.avatar) {
    await cld.uploader.destroy(userDoc.avatar);
  }

  await User.updateOne({ userName: req.user.userName }, { $set: { avatar: null } });

  res.json({
    success: true,
    results: {
      message: 'profile picture has been deleted successfully!',
    }
  })
});

module.exports.getUserCoverPhoto = catchAsync(async (req, res) => {

  const doc = await User.findOne({ userName: req.params.profileId }, { cover: 1, _id: 0 });

  res.json({
    success: true,
    results: doc,
  });
});

module.exports.updateUserCoverPhoto = catchAsync(async (req, res, next) => {
  const userDoc = await User.findOne({ userName: req.user.userName }, { cover: 1, _id: 0 });

  // fixme: doesn't delete
  if (userDoc.cover)
    await cld.uploader.destroy(userDoc.cover);

  // store new image path in mongodb
  const newData = {
    cover: req.files[0].path,
  }
  await User.updateOne({ userName: req.user.userName }, { $set: newData });

  res.json({
    success: true,
    results: newData,
  })

});

module.exports.deleteUserCoverPhoto = catchAsync(async (req, res) => {
  const userDoc = await User.findOne({ userName: req.user.userName }, { cover: 1, _id: 0 });
  // fixme: doesn't delete
  if (userDoc.cover)
    await cld.uploader.destroy(userDoc.cover);

  await User.updateOne({ userName: req.user.userName }, { $set: { cover: null } });

  res.json({
    success: true,
    results: {
      message: 'cover picture has been deleted successfully!',
    }
  })
});


module.exports.getSomeUserPhotos = catchAsync(async (req, res) => {
  // todo
  res.send('getSomeUserPhotos function');
});

module.exports.getSomeUserVideos = catchAsync(async (req, res) => {
  // todo
  res.send('getSomeUserVideos function');
});


module.exports.block = catchAsync(async (req, res, next) => {
  // source wants to block sink
  const source = req.user.userName;
  const sink = req.params.profileId;

  // todo: blockList must have maximum length

  await List.updateOne({ _id: source }, {
    // note addToSet is not necessary here
    // as guard module will make sure that sink is not blocked already
    $push: {
      blockList: sink,
    }
  });

  res.json({
    success: true,
    results: {
      message: `\`${sink}\` has been blocked successfully!`,
    }
  })
});



module.exports.unblock = catchAsync(async (req, res, next) => {
  // source wants to unblock sink
  const source = req.user.userName;
  const sink = req.params.profileId;

  // todo: what if sink is not blocked

  await List.updateOne({ _id: source }, {
    // note addToSet is not necessary here
    // as guard module will make sure that sink is not blocked already
    $pull: {
      blockList: sink,
    }
  });

  res.json({
    success: true,
    results: {
      message: `You unblocked \`${sink}\` successfully!`,
    }
  })
});



