const catchAsync = require('../utils/catchAsync');
const axios = require('axios');
const Amadeus = require('amadeus');
const _ = require('lodash');
const City = require('../models/city');
const ClientError = require('../utils/ClientError');

module.exports.getCityInfo = catchAsync(async (req, res, next) => {
    const city = await City.findOne({ _id: req.params.profileId }).lean();

    if (!city) {
        return next(new ClientError('Invalid city id', res.STATUS_CODE.BadRequest));
    }

    city.country = (await axios({
        method: 'GET',
        url: `${req.protocol}://${req.get('host')}/api/v1/countries/${city.country}`,
        headers: {
            'access-token': req.headers['access-token'],
        }
    })).data.results;

    city.isAdmin = city.admins.includes(req.user.userName);
    delete city.admins;

    res.json({
        success: true,
        results: city,
    });
});

module.exports.getCityActivities = catchAsync(async (req, res, next) => {
    const cityInfo = (await axios({
        method: 'GET',
        url: `${req.protocol}://${req.get('host')}/api/v1/cities/${req.params.profileId}`,
        headers: {
            'access-token': req.headers['access-token'],
        }
    })).data?.results;

    if (!cityInfo) {
        return next(new ClientError("There is no city with this ID!", res.STATUS_CODE.BadRequest));
    }

    const amadeus = new Amadeus({
        clientId: process.env.AMADEUS_CLIENT_ID,
        clientSecret: process.env.AMADEUS_CLIENT_SECRET,
    });

    const response = await amadeus.client.get('/v1/shopping/activities', {
        latitude: cityInfo.location.latitude,
        longitude: cityInfo.location.longitude,
        radius: 3,
    });

    if (!response?.data) {
        return next(new ClientError("Failed to get the city activities!", res.STATUS_CODE.InternalServerError));
    }

    response.data.forEach(el => {
        delete el.self;
    });

    res.json({
        success: true,
        results: { data: response.data },
    });
});

module.exports.getCityHotels = catchAsync(async (req, res, next) => {
    let data = (await axios({
        method: 'GET',
        url: 'https://booking-com.p.rapidapi.com/v1/hotels/search',
        params: {
            filter_by_currency: 'USD',
            order_by: 'popularity',
            locale: 'en-gb',
            units: 'metric',
            page_number: '0',
            dest_type: 'city',
            dest_id: req.params.profileId,
            ...(req.query),
        },
        headers: {
            'X-RapidAPI-Host': process.env.X_RAPIDAPI_HOST,
            'X-RapidAPI-Key': process.env.X_RAPIDAPI_KEY,
        }
    })).data?.result;
    data = [
        {
            "url": "https://www.booking.com/hotel/eg/welcome-pyramids-view.html",
            "hotel_name": "Number One Pyramids Hotel",
            "hotel_id": 8150100,
            "address": "50 جمال عبد الناصر",
            "review_score": 9.6,
            "review_score_word": "Exceptional",
            "main_photo_url": "https://cf.bstatic.com/xdata/images/hotel/square60/350898737.jpg?k=c88a87cc224e864fdb07e82e4bf001b48801b41d94344c9c4f39bb779b216cb7&o=",
            "review_nr": 20,
            "checkin": {
                "from": "00:00",
                "until": "00:00"
            },
            "checkout": {
                "from": "00:00",
                "until": "00:00"
            }
        },
        {
            "url": "https://www.booking.com/hotel/eg/wassim.html",
            "hotel_name": "Wassim Boutique",
            "review_score": 7.7,
            "checkin": {
                "until": "12:00",
                "from": "11:30"
            },
            "checkout": {
                "until": "12:00",
                "from": "10:00"
            },
            "hotel_id": 8078654,
            "review_score_word": "Good",
            "main_photo_url": "https://cf.bstatic.com/xdata/images/hotel/square60/339467631.jpg?k=30c341ad819092e03f92172787137c5eca5b8db2ffe4726e17d115ff7a075b74&o=",
            "review_nr": 55,
            "address": "6 El Tahrir"
        },
        {
            "url": "https://www.booking.com/hotel/eg/marvel-stone-al-haram.html",
            "hotel_name": "Marvel Stone Hotel",
            "review_score": 8,
            "checkin": {
                "until": "",
                "from": "13:00"
            },
            "checkout": {
                "until": "12:00",
                "from": ""
            },
            "hotel_id": 2744003,
            "review_score_word": "Very good",
            "main_photo_url": "https://cf.bstatic.com/xdata/images/hotel/square60/291904163.jpg?k=e9deebe74a1ad15e1d5e6f27d5395210cef8c0cc860e40ed41e0c400f1f9228e&o=",
            "review_nr": 1152,
            "address": "6 gerier street, nazlet elsemman,giza"
        },
        {
            "url": "https://www.booking.com/hotel/eg/pyramids-planet.html",
            "hotel_name": "Pyramids Planet Hotel",
            "review_score": 9.6,
            "checkin": {
                "from": "11:30",
                "until": "12:00"
            },
            "checkout": {
                "from": "11:30",
                "until": "12:00"
            },
            "hotel_id": 6997023,
            "review_score_word": "Exceptional",
            "main_photo_url": "https://cf.bstatic.com/xdata/images/hotel/square60/295936765.jpg?k=059b489f211ca31420ab95a77e05b7ec02367f1dbba430ba85c27ddf7fc1eea8&o=",
            "review_nr": 311,
            "address": "198 W , Hadayek El Ahram , Gate 2"
        },
        {
            "url": "https://www.booking.com/hotel/eg/pyramidstarsinn.html",
            "hotel_name": "Pyramid Stars Inn",
            "review_score": 8.3,
            "checkin": {
                "from": "12:30",
                "until": "23:30"
            },
            "checkout": {
                "until": "12:30",
                "from": ""
            },
            "hotel_id": 8144542,
            "review_score_word": "Very good",
            "main_photo_url": "https://cf.bstatic.com/xdata/images/hotel/square60/346064631.jpg?k=640327174b46e384db949549c08a6d4f506381ee8e71b6d12c7aa9fc069c9196&o=",
            "review_nr": 66,
            "address": "21 St Elmaleka Farida Elmansorya"
        },
        {
            "url": "https://www.booking.com/hotel/eg/sphinx-golden-gate-inn.html",
            "hotel_name": "Sphinx Golden Gate Inn",
            "review_score": 8.4,
            "checkin": {
                "from": "14:00",
                "until": "00:00"
            },
            "checkout": {
                "from": "12:00",
                "until": "13:00"
            },
            "hotel_id": 3703056,
            "review_score_word": "Very good",
            "main_photo_url": "https://cf.bstatic.com/xdata/images/hotel/square60/324275912.jpg?k=e5b7efd83e1a14624fcd3cd570ca1f2adf993fcb82bb00198754c3d26c71e746&o=",
            "review_nr": 154,
            "address": "9sphinx street behind saman hospital"
        },
        {
            "url": "https://www.booking.com/hotel/eg/jimmy-39-s-pyramids-inn.html",
            "hotel_name": "Jimmy Pyramids View",
            "review_score": 8.7,
            "checkin": {
                "from": "12:00",
                "until": "14:00"
            },
            "checkout": {
                "from": "12:00",
                "until": "13:00"
            },
            "hotel_id": 7767167,
            "review_score_word": "Fabulous",
            "main_photo_url": "https://cf.bstatic.com/xdata/images/hotel/square60/324445360.jpg?k=0f9368a725eeb80313e0f355ff678916c5dbcbd6b188719c2b931f3695e34e14&o=",
            "review_nr": 100,
            "address": "Kaab Ibn Malek"
        },
        {
            "url": "https://www.booking.com/hotel/eg/horus-guest-house-apartment.html",
            "hotel_name": "Horus Guest House Pyramids View",
            "review_score": 9.2,
            "checkin": {
                "from": "14:00",
                "until": "00:00"
            },
            "checkout": {
                "until": "11:00",
                "from": "07:00"
            },
            "hotel_id": 1641389,
            "review_score_word": "Superb",
            "main_photo_url": "https://cf.bstatic.com/xdata/images/hotel/square60/137847863.jpg?k=7c0fb217582d758dac647bfea974ba279555b78569776dd3de699778504fc952&o=",
            "review_nr": 807,
            "address": "21 Gamal Abd El Nasir Street From Abou Al Hool Al Seiahi Street, Giza"
        },
        {
            "url": "https://www.booking.com/hotel/eg/cairo-house-abdeen1.html",
            "hotel_name": "Cairo House",
            "review_score": 7.6,
            "checkin": {
                "until": "18:00",
                "from": "12:00"
            },
            "checkout": {
                "until": "11:00",
                "from": ""
            },
            "hotel_id": 6894982,
            "review_score_word": "Good",
            "main_photo_url": "https://cf.bstatic.com/xdata/images/hotel/square60/306371726.jpg?k=00e30582e1956ffd02deda6034dacfd8e4a11b4bd65048c1591f07dce3131cf6&o=",
            "review_nr": 115,
            "address": "20 Adly"
        },
        {
            "url": "https://www.booking.com/hotel/eg/pyramids-top.html",
            "hotel_name": "Pyramids Top Inn",
            "review_score": 9.3,
            "checkin": {
                "from": "14:00",
                "until": "22:00"
            },
            "checkout": {
                "until": "13:00",
                "from": "10:00"
            },
            "hotel_id": 4816083,
            "review_score_word": "Superb",
            "main_photo_url": "https://cf.bstatic.com/xdata/images/hotel/square60/331340441.jpg?k=80531770a1089440d3ffc71cdd45cabc939644fbd3e2170dc319c2cbeff83940&o=",
            "review_nr": 204,
            "address": "Abu elhol street pyramids area 15"
        },
        {
            "url": "https://www.booking.com/hotel/eg/atlantis-pyramids-inn.html",
            "hotel_name": "Atlantis pyramids inn",
            "review_score": 9.3,
            "checkin": {
                "until": "22:00",
                "from": "12:00"
            },
            "checkout": {
                "until": "13:00",
                "from": "12:00"
            },
            "hotel_id": 4235072,
            "review_score_word": "Superb",
            "main_photo_url": "https://cf.bstatic.com/xdata/images/hotel/square60/172404239.jpg?k=18d5a1be5936d833c2678ad9544d76d57319d1ae0dd57a998986980ef0316a5f&o=",
            "review_nr": 589,
            "address": "Sphinx square pyramids giza 4 sphinx street pyramids giza"
        },
        {
            "url": "https://www.booking.com/hotel/eg/el-tahrir-apartment.html",
            "hotel_name": "Valencia Hotel, Downtown Cairo",
            "review_score": 8.4,
            "checkin": {
                "until": "12:00",
                "from": "12:00"
            },
            "checkout": {
                "until": "11:30",
                "from": "10:00"
            },
            "hotel_id": 5722458,
            "review_score_word": "Very good",
            "main_photo_url": "https://cf.bstatic.com/xdata/images/hotel/square60/298147216.jpg?k=0e290eaff548759c3b54927c4b6f88ca20d4f640480a2c2b6bb65c184c4180d9&o=",
            "review_nr": 584,
            "address": "1 Seket El Fadl, Talaat Harb Street, Downtown Cairo"
        },
        {
            "url": "https://www.booking.com/hotel/eg/egypt-pyramids-inn-giza.html",
            "hotel_name": "Egypt pyramids inn",
            "review_score": 8.8,
            "checkin": {
                "until": "00:00",
                "from": "13:00"
            },
            "checkout": {
                "until": "12:00",
                "from": "00:00"
            },
            "hotel_id": 4081435,
            "review_score_word": "Fabulous",
            "main_photo_url": "https://cf.bstatic.com/xdata/images/hotel/square60/279007538.jpg?k=fde7087ca82e834d2f279e874dc1dfc9441e594780eba0f4a7124380bc4c140d&o=",
            "review_nr": 791,
            "address": "Abu el Holl el Syahiy Street No 1"
        },
        {
            "url": "https://www.booking.com/hotel/eg/the-square-boutique.html",
            "hotel_name": "The Square Boutique Hotel",
            "review_score": 8.3,
            "checkin": {
                "until": "00:00",
                "from": "14:00"
            },
            "checkout": {
                "until": "13:30",
                "from": "11:30"
            },
            "hotel_id": 4389851,
            "review_score_word": "Very good",
            "main_photo_url": "https://cf.bstatic.com/xdata/images/hotel/square60/332506135.jpg?k=8e04748f699fbf6b2d50359b6222c9a6398b22a54caf8a3f3caa0d3ea9980e72&o=",
            "review_nr": 423,
            "address": "Talaat Harb Square Number 3 - 4th Floor"
        },
        {
            "url": "https://www.booking.com/hotel/eg/nakhil-residence.html",
            "hotel_name": "Nakhil Inn Residence",
            "review_score": 8.6,
            "checkin": {
                "until": "23:00",
                "from": "13:30"
            },
            "checkout": {
                "from": "12:00",
                "until": "12:30"
            },
            "hotel_id": 2296105,
            "review_score_word": "Fabulous",
            "main_photo_url": "https://cf.bstatic.com/xdata/images/hotel/square60/250812789.jpg?k=4c432f58f2258b40c03b98901320c1ebb3f16e0e81ae7e0a94c0d0faa0236a01&o=",
            "review_nr": 542,
            "address": "3 Al Nakhel"
        },
        {
            "url": "https://www.booking.com/hotel/eg/the-museum-boutique-al-haram.html",
            "hotel_name": "The Museum Boutique",
            "review_score": 8.8,
            "checkin": {
                "until": "16:30",
                "from": "08:30"
            },
            "checkout": {
                "until": "15:00",
                "from": "05:00"
            },
            "hotel_id": 7252692,
            "review_score_word": "Fabulous",
            "main_photo_url": "https://cf.bstatic.com/xdata/images/hotel/square60/318029521.jpg?k=4b5c8414fc1404688e8aaf554bec3e679818b56ebba13c2c7bbc7330054c2cc4&o=",
            "review_nr": 64,
            "address": "El Borg Street"
        },
        {
            "url": "https://www.booking.com/hotel/eg/amin.html",
            "hotel_name": "Amin Hotel",
            "review_score": 8,
            "checkin": {
                "until": "",
                "from": "13:00"
            },
            "checkout": {
                "until": "12:00",
                "from": ""
            },
            "hotel_id": 2139214,
            "review_score_word": "Very good",
            "main_photo_url": "https://cf.bstatic.com/xdata/images/hotel/square60/258570813.jpg?k=9a1687d46ca93ba4e85250f9e6a74dce6c2faa1dba8f330330450999a222c60e&o=",
            "review_nr": 871,
            "address": "Falaky Square 38"
        },
        {
            "url": "https://www.booking.com/hotel/eg/elite-pyramids-boutique.html",
            "hotel_name": "Elite Pyramids Boutique Hotel",
            "review_score": 9.3,
            "checkin": {
                "from": "13:00",
                "until": "14:00"
            },
            "checkout": {
                "until": "12:00",
                "from": "11:00"
            },
            "hotel_id": 5837054,
            "review_score_word": "Superb",
            "main_photo_url": "https://cf.bstatic.com/xdata/images/hotel/square60/229699613.jpg?k=ab4c13edcac9f587f88732f18595043da795ce04fdc10cdef1120d28fa96a16f&o=",
            "review_nr": 338,
            "address": "5 HADAYEK ALAHRAM. ALHARAM ST.. GIZA"
        },
        {
            "url": "https://www.booking.com/hotel/eg/aladdin-lhrm.html",
            "hotel_name": "Seven Eleven Hotel",
            "review_score": 7.8,
            "checkin": {
                "from": "12:30",
                "until": "00:00"
            },
            "checkout": {
                "until": "13:00",
                "from": "12:00"
            },
            "hotel_id": 7771565,
            "review_score_word": "Good",
            "main_photo_url": "https://cf.bstatic.com/xdata/images/hotel/square60/331693591.jpg?k=5c6ba487135aad90886028c1775f1b57e3b3706200f73864af36f278b14466ba&o=",
            "review_nr": 46,
            "address": "8 Sameh Abdel Hakam Street, off Al Gaish Road, Abo Al Hoal AlSeyahy, Mansouria, Haram, Giza"
        },
        {
            "url": "https://www.booking.com/hotel/eg/nine-pyramids-view.html",
            "hotel_name": "Nine Pyramids View Hotel",
            "review_score": 8.2,
            "checkin": {
                "until": "12:00",
                "from": "12:00"
            },
            "checkout": {
                "until": "10:00",
                "from": ""
            },
            "hotel_id": 7061106,
            "review_score_word": "Very good",
            "main_photo_url": "https://cf.bstatic.com/xdata/images/hotel/square60/285060525.jpg?k=34dc9ac72b2096643e14894ed5bb9796bdccd1eefe541ddb248efe0496859b82&o=",
            "review_nr": 438,
            "address": "Abou Al Hool Al Seiahi 2st, mohamed hassab salam st mansoria haram"
        }
    ];

    if (!data) {
        return next(new ClientError("failed to get data, please try again later!", res.STATUS_CODE.InternalServerError));
    }

    const final_data = data.map(el => _.pick(el, ['url', 'hotel_name', 'checkin', 'checkout', 'hotel_id', 'review_score', 'review_score_word', 'review_nr', 'address', 'min_total_price', 'main_photo_url', 'max_photo_url', 'max_1440_photo_url']));

    res.json({
        success: true,
        results: {
            data: final_data,
        }
    });

});