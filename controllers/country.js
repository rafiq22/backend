const catchAsync = require('../utils/catchAsync');
const Country = require('../models/country');

module.exports.getCountryInfo = catchAsync(async (req, res, next) => {
    const doc = await Country.findOne({ _id: req.params.countryId });
    res.json({
        success: true,
        results: doc,
    });
});