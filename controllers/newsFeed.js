const catchAsync = require('../utils/catchAsync');
const ClientError = require('../utils/ClientError.js');

const User = require('../models/users.js');

module.exports.get = catchAsync(async (req, res) => {
  const { newsFeed } = await User.getNewsFeed(req.user.userName);

  res.json({
    newsFeed: newsFeed,
  });
});