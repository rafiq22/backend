const mongoose = require('mongoose');
// built-in
const http = require('http');
// from npm
const dotenv = require('dotenv');
// our own
const app = require('./app.js');

dotenv.config({ path: './.env' });

// connecting to the MongoDB Database
const DB = process.env.DATABASE;
mongoose
  .connect(DB)
  .then(() => console.log('Connected to The Cloud MongoDB successfully'))
  .catch((err) => console.log('DB Connection Failed!', err));

const httpServer = http.createServer(app);
const port = process.env.PORT || 8000;
httpServer.listen(port, () => {
  console.log(`start listening on port ${port}`);
});
