# import the necessary packages
from pyimagesearch.colordescriptor import ColorDescriptor
from pyimagesearch.searcher import Searcher
import cv2
import os
import sys
import math

# initialize the image descriptor
cd = ColorDescriptor((8, 12, 3))

# load the query image and describe it                    
queryImageAbsPath = os.path.normpath(f'{os.path.abspath(__file__)}/../queries/{sys.argv[1]}')
queryImage = cv2.imread(queryImageAbsPath)
features = cd.describe(queryImage)

# perform the search
searcher = Searcher(os.path.normpath(f'{os.path.abspath(__file__)}/../index.csv'))
results = searcher.search(features)

best_score = -1
for (score, resultID) in results:
    # load the result image and display it
    if best_score == -1:
        best_score = score
    if math.floor(score) == math.floor(best_score):
      print(resultID)
