const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');

// for security purposes
const helmet = require('helmet');
const rateLimit = require('express-rate-limit');
const xssClean = require('xss-clean');
const cors = require('cors');

const api_v1_router = require('./routers/api_v1MasterRouter');
const globalErrorHandler = require('./controllers/errorHandler.js');

const ClientError = require('./utils/ClientError.js');
const { STATUS_CODE } = require('./utils/constants.js');

const path = require('path');


const app = express();

// this middleware to struggle brute-force attacks
// const limiter = rateLimit({
//   // maximum 5 request every 2-seconds
//   max: 5,
//   windowMs: 2 * 1000,
//   message: 'Too many requests in a every short time!',
// });
// app.use('/api', limiter);

// reading data from the body into req.body
app.use(bodyParser.json());
app.use(cookieParser());

// data sanitization against XSS attacks
app.use(xssClean());

app.options('*', cors());
app.use(cors());

// for securing http headers
app.use(helmet({
  contentSecurityPolicy: false,
}));

// app.use(function (req, res, next) {
//   res.setHeader('Access-Control-Allow-Origin', '*');
//   res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
//   res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
//   res.setHeader('Access-Control-Allow-Credentials', true);
//   next();
// });

// app.use(createProxyMiddleware({ target: "*" }));

// serving static files
// app.use(express.static(`${__dirname}/public`));

app.use((req, res, next) => {
  res.STATUS_CODE = STATUS_CODE;
  next();
});

app.use('/api/v1', api_v1_router);
app.use(express.static(path.join(__dirname, 'public')));

// app.get('/favicon.ico', (req, res) => {
//   // todo: how status code on the browser is 200 and type is html ??
//   res.sendStatus(res.STATUS_CODE.NotFound);
// });


app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'public/index.html'));
  // res.send('welcome to Rafiq');
});

app.all('*', (req, res, next) => {
  next(
    new ClientError(
      `Can't find ${req.originalUrl} on this server!`,
      STATUS_CODE.NotFound
    )
  );
});

app.use(globalErrorHandler);

module.exports = app;
