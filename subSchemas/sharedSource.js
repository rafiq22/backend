const mongoose = require('mongoose');
const User = require('../models/users');

const UserSchema = User.schema.obj;
const userNameProperties = { ...UserSchema.userName };
const firstNameProperties = {...UserSchema.firstName};
const lastNameProperties = {...UserSchema.lastName};
const avatarProperties = {...UserSchema.avatar};

delete userNameProperties.unique;
delete userNameProperties.index;
delete userNameProperties.required;
delete firstNameProperties.required;
delete lastNameProperties.required;

const sharedSourceSchema = new mongoose.Schema({
    authorInfo: {
        userName: { ...userNameProperties },
        firstName: { ...firstNameProperties },
        lastName: { ...lastNameProperties },
        avatar: { ...avatarProperties }, 
    },
    postId: {
        type: String,
        default: null,
    }
});

module.exports = sharedSourceSchema;