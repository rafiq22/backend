const mongoose = require('mongoose');

const travelMapSchema = new mongoose.Schema({
    latitude: {
        type: Number,
        required: true,
    },
    longitude: {
        type: Number,
        required: true,
    },
    type: {
        type: String,
        required: true,
        lowercase: true,
        enum: ['done', 'wish'],
    }
});

module.exports = travelMapSchema;