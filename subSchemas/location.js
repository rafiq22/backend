const mongoose = require('mongoose');

const User = require('../models/users');
const ClientError = require('../utils/ClientError');
const STATUS_CODE = require('../utils/constants');
const City = require('../models/city');
const Country = require('../models/country');

const UserSchema = User.schema.obj;
const userNameProperties = { ...UserSchema.userName };

delete userNameProperties.unique;
delete userNameProperties.index;


const locationSchema = new mongoose.Schema({
    name: { ...userNameProperties },
    coordinates: {
        type: String,
        default: "",
        maxlength: 1024,
    },
    description: {
        type: String,
        default: "",
        maxlength: 1024,
    },
    transportation: {
        type: String,
        default: "",
        maxlength: 1024,
    },
    price: {
        type: Number,
        default: 0,
        min: 0,
    },


});



module.exports = Location = mongoose.model('Locations', locationSchema);