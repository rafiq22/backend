const mongoose = require('mongoose');

const contentSchema = new mongoose.Schema({
    text: {
        type: String,
        default: "",
        maxlength: 1024, //todo: decide max number of characters
    },
    files: [String],
});

module.exports = contentSchema;