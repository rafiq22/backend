const mongoose = require('mongoose');

const socialMediaSchema = new mongoose.Schema({
    userName: {
        type: String,
        required: true,
    },
    label: {
        type: String,
        required: true,
    },
}, { toJSON: { virtuals: true } });

socialMediaSchema.virtual('link').get(function () {
    const links = [
        'https://www.facebook.com/',
        'https://www.instagram.com/',
        'https://www.youtube.com/',
        'https://www.tiktok.com/@',
    ];

    const website = this.label.toLowerCase();
    for (const link of links) {
        if (link.includes(website)) {
            return `${link}${this.userName}`;
        }
    }
});

module.exports = socialMediaSchema;