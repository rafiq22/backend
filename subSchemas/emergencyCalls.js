const mongoose = require('mongoose');

const emergencyNumbersSchema = mongoose.Schema({
    police: {
        type: [String],
    },
    ambulance: {
        type: [String],
    },
    fire: {
        type: [String],
    },

});

module.exports = emergencyNumbersSchema;