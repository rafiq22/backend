module.exports.STATUS_CODE = Object.freeze({
  // 2xx: HTTP Successful Codes
  OK: 200,
  Created: 201,
  Accepted: 202,
  NoContent: 204,
  ResetContent: 205,
  PartialContent: 206,
  MultiStatus: 207,
  // 3xx: HTTP Redire­ction Codes
  MultipleChoices: 300,
  MovedPermanently: 301,
  Found: 302,
  // 4xx: HTTP Client Error Code
  BadRequest: 400,
  Unauthorized: 401,
  PaymentRequired: 402,
  Forbidden: 403,
  NotFound: 404,
  MethodNotAllowed: 405,
  NotAcceptable: 406,
  ProxyAuthenticationRequired: 407,
  RequestTimeout: 408,
  Conflict: 409, // ex: if user tries to register with exists email or username
  RequestEntityTooLarge: 413,
  // 5xx: HTTP Server Error Codes
  InternalServerError: 500,
  NotImplemented: 501,
  BadGateway: 502,
  ServiceUnavailable: 503,
});

module.exports.countryEnum = ['Afghanistan', 'Aland Islands', 'Albania', 'Algeria', 'American Samoa', 'Andorra', 'Angola', 'Antarctica', 'Antigua and Barbuda', 'Argentina', 'Armenia', 'Aruba', 'Australia', 'Austria', 'Azerbaijan', 'Bahamas', 'Bahrain', 'Bangladesh', 'Barbados', 'Belarus', 'Belgium', 'Belize', 'Benin', 'Bermuda', 'Bhutan', 'Bolivia', 'Bonaire', 'Bosnia and Herzegovina', 'Botswana', 'Bouvet Island', 'Brazil', 'British Indian Ocean Territory', 'British Virgin Islands', 'Brunei', 'Bulgaria', 'Burkina Faso', 'Burundi', 'Cambodia', 'Cameroon', 'Canada', 'Cape Verde', 'Central African Republic', 'Chad', 'Chile', 'China', 'Christmas Island', 'Colombia', 'Comoros', 'Cook Islands', 'Costa Rica', 'Croatia', 'Cuba', 'Curacao', 'Cyprus', 'Czechia', 'Democratic Republic of the Congo', 'Denmark', 'Djibouti', 'Dominican Republic', 'East Timor', 'Ecuador', 'Egypt', 'El Salvador', 'Eritrea', 'Estonia', 'Ethiopia', 'Falkland Islands', 'Faroe Islands', 'Fiji', 'Finland', 'France', 'French Polynesia', 'French Southern Territories', 'Gabon', 'Gambia', 'Georgia', 'Germany', 'Ghana', 'Gibraltar', 'Greece', 'Greenland', 'Grenada', 'Guadeloupe', 'Guam', 'Guatemala', 'Guinea', 'Guinea-Bissau', 'Guyana', 'Haiti', 'Heard Island and McDonald Islands', 'Honduras', 'Hong Kong', 'Hungary', 'Iceland', 'India', 'Indonesia', 'Iran', 'Iraq', 'Ireland', 'Isle of Man', 'Italy', 'Ivory Coast', 'Jamaica', 'Japan', 'Jersey', 'Jordan', 'Kazakhstan', 'Kenya', 'Kiribati', 'Kosovo', 'Kuwait', 'Kyrgyzstan', 'Laos', 'Latvia', 'Lebanon', 'Liberia', 'Libya', 'Liechtenstein', 'Lithuania', 'Luxembourg', 'Macao', 'Macedonia', 'Madagascar', 'Malawi', 'Malaysia', 'Maldives', 'Mali', 'Malta', 'Marshall Islands', 'Martinique', 'Mauritania', 'Mauritius', 'Mexico', 'Micronesia', 'Moldova', 'Monaco', 'Mongolia', 'Montenegro', 'Montserrat', 'Morocco', 'Mozambique', 'Myanmar', 'Namibia', 'Nauru', 'Nepal', 'Netherlands', 'Netherlands Antilles', 'New Zealand', 'Nicaragua', 'Niger', 'Nigeria', 'Niue', 'Norfolk Island', 'North Korea', 'Northern Mariana Islands', 'Norway', 'Oman', 'Pakistan', 'Palau', 'Palestine', 'Paraguay', 'Peru', 'Philippines', 'Pitcairn', 'Poland', 'Portugal', 'Puerto Rico', 'Qatar', 'Romania', 'Russia', 'Rwanda', 'Saint Barthelemy', 'Saint Helena', 'Saint Kitts and Nevis', 'Saint Lucia', 'Saint Pierre and Miquelon', 'Saint Vincent and the Grenadines', 'Samoa', 'San Marino', 'Saudi Arabia', 'Senegal', 'Serbia', 'Serbia and Montenegro', 'Seychelles', 'Sierra Leone', 'Singapore', 'Sint Maarten', 'Slovakia', 'Slovenia', 'Solomon Islands', 'Somalia', 'South Africa', 'South Georgia and the South Sandwich Islands', 'South Korea', 'South Sudan', 'Spain', 'Sri Lanka', 'Sudan', 'Suriname', 'Svalbard and Jan Mayen', 'Swaziland', 'Sweden', 'Switzerland', 'Syria', 'Taiwan', 'Tajikistan', 'Tanzania', 'Thailand', 'Togo', 'Tonga', 'Trinidad and Tobago', 'Tunisia', 'Turkey', 'Turkmenistan', 'Tuvalu', 'U.S. Virgin Islands', 'Uganda', 'Ukraine', 'United Arab Emirates', 'United Kingdom', 'United States', 'United States Minor Outlying Islands', 'Uruguay', 'Uzbekistan', 'Vatican', 'Venezuela', 'Vietnam', 'Western Sahara', 'Yemen', 'Zambia', 'Zimbabwe'];

module.exports.countryList = [
  {
    "name": "Afghanistan",
    "ISOCode": "AF"
  },
  {
    "name": "Åland Islands",
    "ISOCode": "AX"
  },
  {
    "name": "Albania",
    "ISOCode": "AL"
  },
  {
    "name": "Algeria",
    "ISOCode": "DZ"
  },
  {
    "name": "American Samoa",
    "ISOCode": "AS"
  },
  {
    "name": "Andorra",
    "ISOCode": "AD"
  },
  {
    "name": "Angola",
    "ISOCode": "AO"
  },
  {
    "name": "Anguilla",
    "ISOCode": "AI"
  },
  {
    "name": "Antarctica",
    "ISOCode": "AQ"
  },
  {
    "name": "Antigua and Barbuda",
    "ISOCode": "AG"
  },
  {
    "name": "Argentina",
    "ISOCode": "AR"
  },
  {
    "name": "Armenia",
    "ISOCode": "AM"
  },
  {
    "name": "Aruba",
    "ISOCode": "AW"
  },
  {
    "name": "Australia",
    "ISOCode": "AU"
  },
  {
    "name": "Austria",
    "ISOCode": "AT"
  },
  {
    "name": "Azerbaijan",
    "ISOCode": "AZ"
  },
  {
    "name": "Bahamas",
    "ISOCode": "BS"
  },
  {
    "name": "Bahrain",
    "ISOCode": "BH"
  },
  {
    "name": "Bangladesh",
    "ISOCode": "BD"
  },
  {
    "name": "Barbados",
    "ISOCode": "BB"
  },
  {
    "name": "Belarus",
    "ISOCode": "BY"
  },
  {
    "name": "Belgium",
    "ISOCode": "BE"
  },
  {
    "name": "Belize",
    "ISOCode": "BZ"
  },
  {
    "name": "Benin",
    "ISOCode": "BJ"
  },
  {
    "name": "Bermuda",
    "ISOCode": "BM"
  },
  {
    "name": "Bhutan",
    "ISOCode": "BT"
  },
  {
    "name": "Bolivia, Plurinational State of",
    "ISOCode": "BO"
  },
  {
    "name": "Bonaire, Sint Eustatius and Saba",
    "ISOCode": "BQ"
  },
  {
    "name": "Bosnia and Herzegovina",
    "ISOCode": "BA"
  },
  {
    "name": "Botswana",
    "ISOCode": "BW"
  },
  {
    "name": "Bouvet Island",
    "ISOCode": "BV"
  },
  {
    "name": "Brazil",
    "ISOCode": "BR"
  },
  {
    "name": "British Indian Ocean Territory",
    "ISOCode": "IO"
  },
  {
    "name": "Brunei Darussalam",
    "ISOCode": "BN"
  },
  {
    "name": "Bulgaria",
    "ISOCode": "BG"
  },
  {
    "name": "Burkina Faso",
    "ISOCode": "BF"
  },
  {
    "name": "Burundi",
    "ISOCode": "BI"
  },
  {
    "name": "Cape Verde",
    "ISOCode": "CV"
  },
  {
    "name": "Cambodia",
    "ISOCode": "KH"
  },
  {
    "name": "Cameroon",
    "ISOCode": "CM"
  },
  {
    "name": "Canada",
    "ISOCode": "CA"
  },
  {
    "name": "Cayman Islands",
    "ISOCode": "KY"
  },
  {
    "name": "Central African Republic",
    "ISOCode": "CF"
  },
  {
    "name": "Chad",
    "ISOCode": "TD"
  },
  {
    "name": "Chile",
    "ISOCode": "CL"
  },
  {
    "name": "China",
    "ISOCode": "CN"
  },
  {
    "name": "Christmas Island",
    "ISOCode": "CX"
  },
  {
    "name": "Cocos (Keeling) Islands",
    "ISOCode": "CC"
  },
  {
    "name": "Colombia",
    "ISOCode": "CO"
  },
  {
    "name": "Comoros",
    "ISOCode": "KM"
  },
  {
    "name": "Congo",
    "ISOCode": "CG"
  },
  {
    "name": "Congo, the Democratic Republic of the",
    "ISOCode": "CD"
  },
  {
    "name": "Cook Islands",
    "ISOCode": "CK"
  },
  {
    "name": "Costa Rica",
    "ISOCode": "CR"
  },
  {
    "name": "Côte d'Ivoire",
    "ISOCode": "CI"
  },
  {
    "name": "Croatia",
    "ISOCode": "HR"
  },
  {
    "name": "Cuba",
    "ISOCode": "CU"
  },
  {
    "name": "Curaçao",
    "ISOCode": "CW"
  },
  {
    "name": "Cyprus",
    "ISOCode": "CY"
  },
  {
    "name": "Czech Republic",
    "ISOCode": "CZ"
  },
  {
    "name": "Denmark",
    "ISOCode": "DK"
  },
  {
    "name": "Djibouti",
    "ISOCode": "DJ"
  },
  {
    "name": "Dominica",
    "ISOCode": "DM"
  },
  {
    "name": "Dominican Republic",
    "ISOCode": "DO"
  },
  {
    "name": "Ecuador",
    "ISOCode": "EC"
  },
  {
    "name": "Egypt",
    "ISOCode": "EG"
  },
  {
    "name": "El Salvador",
    "ISOCode": "SV"
  },
  {
    "name": "Equatorial Guinea",
    "ISOCode": "GQ"
  },
  {
    "name": "Eritrea",
    "ISOCode": "ER"
  },
  {
    "name": "Estonia",
    "ISOCode": "EE"
  },
  {
    "name": "Ethiopia",
    "ISOCode": "ET"
  },
  {
    "name": "Falkland Islands (Malvinas)",
    "ISOCode": "FK"
  },
  {
    "name": "Faroe Islands",
    "ISOCode": "FO"
  },
  {
    "name": "Fiji",
    "ISOCode": "FJ"
  },
  {
    "name": "Finland",
    "ISOCode": "FI"
  },
  {
    "name": "France",
    "ISOCode": "FR"
  },
  {
    "name": "French Guiana",
    "ISOCode": "GF"
  },
  {
    "name": "French Polynesia",
    "ISOCode": "PF"
  },
  {
    "name": "French Southern Territories",
    "ISOCode": "TF"
  },
  {
    "name": "Gabon",
    "ISOCode": "GA"
  },
  {
    "name": "Gambia",
    "ISOCode": "GM"
  },
  {
    "name": "Georgia",
    "ISOCode": "GE"
  },
  {
    "name": "Germany",
    "ISOCode": "DE"
  },
  {
    "name": "Ghana",
    "ISOCode": "GH"
  },
  {
    "name": "Gibraltar",
    "ISOCode": "GI"
  },
  {
    "name": "Greece",
    "ISOCode": "GR"
  },
  {
    "name": "Greenland",
    "ISOCode": "GL"
  },
  {
    "name": "Grenada",
    "ISOCode": "GD"
  },
  {
    "name": "Guadeloupe",
    "ISOCode": "GP"
  },
  {
    "name": "Guam",
    "ISOCode": "GU"
  },
  {
    "name": "Guatemala",
    "ISOCode": "GT"
  },
  {
    "name": "Guernsey",
    "ISOCode": "GG"
  },
  {
    "name": "Guinea",
    "ISOCode": "GN"
  },
  {
    "name": "Guinea-Bissau",
    "ISOCode": "GW"
  },
  {
    "name": "Guyana",
    "ISOCode": "GY"
  },
  {
    "name": "Haiti",
    "ISOCode": "HT"
  },
  {
    "name": "Heard Island and McDonald Islands",
    "ISOCode": "HM"
  },
  {
    "name": "Vatican",
    "ISOCode": "VA"
  },
  {
    "name": "Honduras",
    "ISOCode": "HN"
  },
  {
    "name": "Hong Kong",
    "ISOCode": "HK"
  },
  {
    "name": "Hungary",
    "ISOCode": "HU"
  },
  {
    "name": "Iceland",
    "ISOCode": "IS"
  },
  {
    "name": "India",
    "ISOCode": "IN"
  },
  {
    "name": "Indonesia",
    "ISOCode": "ID"
  },
  {
    "name": "Iran, Islamic Republic of",
    "ISOCode": "IR"
  },
  {
    "name": "Iraq",
    "ISOCode": "IQ"
  },
  {
    "name": "Ireland",
    "ISOCode": "IE"
  },
  {
    "name": "Isle of Man",
    "ISOCode": "IM"
  },
  {
    "name": "Israel",
    "ISOCode": "IL"
  },
  {
    "name": "Italy",
    "ISOCode": "IT"
  },
  {
    "name": "Jamaica",
    "ISOCode": "JM"
  },
  {
    "name": "Japan",
    "ISOCode": "JP"
  },
  {
    "name": "Jersey",
    "ISOCode": "JE"
  },
  {
    "name": "Jordan",
    "ISOCode": "JO"
  },
  {
    "name": "Kazakhstan",
    "ISOCode": "KZ"
  },
  {
    "name": "Kenya",
    "ISOCode": "KE"
  },
  {
    "name": "Kiribati",
    "ISOCode": "KI"
  },
  {
    "name": "Korea, Democratic People's Republic of",
    "ISOCode": "KP"
  },
  {
    "name": "Korea, Republic of",
    "ISOCode": "KR"
  },
  {
    "name": "Kuwait",
    "ISOCode": "KW"
  },
  {
    "name": "Kyrgyzstan",
    "ISOCode": "KG"
  },
  {
    "name": "Lao People's Democratic Republic",
    "ISOCode": "LA"
  },
  {
    "name": "Latvia",
    "ISOCode": "LV"
  },
  {
    "name": "Lebanon",
    "ISOCode": "LB"
  },
  {
    "name": "Lesotho",
    "ISOCode": "LS"
  },
  {
    "name": "Liberia",
    "ISOCode": "LR"
  },
  {
    "name": "Libya",
    "ISOCode": "LY"
  },
  {
    "name": "Liechtenstein",
    "ISOCode": "LI"
  },
  {
    "name": "Lithuania",
    "ISOCode": "LT"
  },
  {
    "name": "Luxembourg",
    "ISOCode": "LU"
  },
  {
    "name": "Macau",
    "ISOCode": "MO"
  },
  {
    "name": "Macedonia, the former Yugoslav Republic of",
    "ISOCode": "MK"
  },
  {
    "name": "Madagascar",
    "ISOCode": "MG"
  },
  {
    "name": "Malawi",
    "ISOCode": "MW"
  },
  {
    "name": "Malaysia",
    "ISOCode": "MY"
  },
  {
    "name": "Maldives",
    "ISOCode": "MV"
  },
  {
    "name": "Mali",
    "ISOCode": "ML"
  },
  {
    "name": "Malta",
    "ISOCode": "MT"
  },
  {
    "name": "Marshall Islands",
    "ISOCode": "MH"
  },
  {
    "name": "Martinique",
    "ISOCode": "MQ"
  },
  {
    "name": "Mauritania",
    "ISOCode": "MR"
  },
  {
    "name": "Mauritius",
    "ISOCode": "MU"
  },
  {
    "name": "Mayotte",
    "ISOCode": "YT"
  },
  {
    "name": "Mexico",
    "ISOCode": "MX"
  },
  {
    "name": "Micronesia, Federated States of",
    "ISOCode": "FM"
  },
  {
    "name": "Moldova, Republic of",
    "ISOCode": "MD"
  },
  {
    "name": "Monaco",
    "ISOCode": "MC"
  },
  {
    "name": "Mongolia",
    "ISOCode": "MN"
  },
  {
    "name": "Montenegro",
    "ISOCode": "ME"
  },
  {
    "name": "Montserrat",
    "ISOCode": "MS"
  },
  {
    "name": "Morocco",
    "ISOCode": "MA"
  },
  {
    "name": "Mozambique",
    "ISOCode": "MZ"
  },
  {
    "name": "Myanmar",
    "ISOCode": "MM"
  },
  {
    "name": "Namibia",
    "ISOCode": "NA"
  },
  {
    "name": "Nauru",
    "ISOCode": "NR"
  },
  {
    "name": "Nepal",
    "ISOCode": "NP"
  },
  {
    "name": "Netherlands",
    "ISOCode": "NL"
  },
  {
    "name": "New Caledonia",
    "ISOCode": "NC"
  },
  {
    "name": "New Zealand",
    "ISOCode": "NZ"
  },
  {
    "name": "Nicaragua",
    "ISOCode": "NI"
  },
  {
    "name": "Niger",
    "ISOCode": "NE"
  },
  {
    "name": "Nigeria",
    "ISOCode": "NG"
  },
  {
    "name": "Niue",
    "ISOCode": "NU"
  },
  {
    "name": "Norfolk Island",
    "ISOCode": "NF"
  },
  {
    "name": "Northern Mariana Islands",
    "ISOCode": "MP"
  },
  {
    "name": "Norway",
    "ISOCode": "NO"
  },
  {
    "name": "Oman",
    "ISOCode": "OM"
  },
  {
    "name": "Pakistan",
    "ISOCode": "PK"
  },
  {
    "name": "Palau",
    "ISOCode": "PW"
  },
  {
    "name": "Palestine, State of",
    "ISOCode": "PS"
  },
  {
    "name": "Panama",
    "ISOCode": "PA"
  },
  {
    "name": "Papua New Guinea",
    "ISOCode": "PG"
  },
  {
    "name": "Paraguay",
    "ISOCode": "PY"
  },
  {
    "name": "Peru",
    "ISOCode": "PE"
  },
  {
    "name": "Philippines",
    "ISOCode": "PH"
  },
  {
    "name": "Pitcairn",
    "ISOCode": "PN"
  },
  {
    "name": "Poland",
    "ISOCode": "PL"
  },
  {
    "name": "Portugal",
    "ISOCode": "PT"
  },
  {
    "name": "Puerto Rico",
    "ISOCode": "PR"
  },
  {
    "name": "Qatar",
    "ISOCode": "QA"
  },
  {
    "name": "Réunion",
    "ISOCode": "RE"
  },
  {
    "name": "Romania",
    "ISOCode": "RO"
  },
  {
    "name": "Russian Federation",
    "ISOCode": "RU"
  },
  {
    "name": "Rwanda",
    "ISOCode": "RW"
  },
  {
    "name": "Saint Barthélemy",
    "ISOCode": "BL"
  },
  {
    "name": "Saint Helena, Ascension and Tristan da Cunha",
    "ISOCode": "SH"
  },
  {
    "name": "Saint Kitts and Nevis",
    "ISOCode": "KN"
  },
  {
    "name": "Saint Lucia",
    "ISOCode": "LC"
  },
  {
    "name": "Saint Martin (French part)",
    "ISOCode": "MF"
  },
  {
    "name": "Saint Pierre and Miquelon",
    "ISOCode": "PM"
  },
  {
    "name": "Saint Vincent and the Grenadines",
    "ISOCode": "VC"
  },
  {
    "name": "Samoa",
    "ISOCode": "WS"
  },
  {
    "name": "San Marino",
    "ISOCode": "SM"
  },
  {
    "name": "Sao Tome and Principe",
    "ISOCode": "ST"
  },
  {
    "name": "Saudi Arabia",
    "ISOCode": "SA"
  },
  {
    "name": "Senegal",
    "ISOCode": "SN"
  },
  {
    "name": "Serbia",
    "ISOCode": "RS"
  },
  {
    "name": "Seychelles",
    "ISOCode": "SC"
  },
  {
    "name": "Sierra Leone",
    "ISOCode": "SL"
  },
  {
    "name": "Singapore",
    "ISOCode": "SG"
  },
  {
    "name": "Sint Maarten (Dutch part)",
    "ISOCode": "SX"
  },
  {
    "name": "Slovakia",
    "ISOCode": "SK"
  },
  {
    "name": "Slovenia",
    "ISOCode": "SI"
  },
  {
    "name": "Solomon Islands",
    "ISOCode": "SB"
  },
  {
    "name": "Somalia",
    "ISOCode": "SO"
  },
  {
    "name": "South Africa",
    "ISOCode": "ZA"
  },
  {
    "name": "South Georgia and the South Sandwich Islands",
    "ISOCode": "GS"
  },
  {
    "name": "South Sudan",
    "ISOCode": "SS"
  },
  {
    "name": "Spain",
    "ISOCode": "ES"
  },
  {
    "name": "Sri Lanka",
    "ISOCode": "LK"
  },
  {
    "name": "Sudan",
    "ISOCode": "SD"
  },
  {
    "name": "Suriname",
    "ISOCode": "SR"
  },
  {
    "name": "Svalbard and Jan Mayen",
    "ISOCode": "SJ"
  },
  {
    "name": "Swaziland",
    "ISOCode": "SZ"
  },
  {
    "name": "Sweden",
    "ISOCode": "SE"
  },
  {
    "name": "Switzerland",
    "ISOCode": "CH"
  },
  {
    "name": "Syrian Arab Republic",
    "ISOCode": "SY"
  },
  {
    "name": "Taiwan, Province of China",
    "ISOCode": "TW"
  },
  {
    "name": "Tajikistan",
    "ISOCode": "TJ"
  },
  {
    "name": "Tanzania, United Republic of",
    "ISOCode": "TZ"
  },
  {
    "name": "Thailand",
    "ISOCode": "TH"
  },
  {
    "name": "Timor-Leste",
    "ISOCode": "TL"
  },
  {
    "name": "Togo",
    "ISOCode": "TG"
  },
  {
    "name": "Tokelau",
    "ISOCode": "TK"
  },
  {
    "name": "Tonga",
    "ISOCode": "TO"
  },
  {
    "name": "Trinidad and Tobago",
    "ISOCode": "TT"
  },
  {
    "name": "Tunisia",
    "ISOCode": "TN"
  },
  {
    "name": "Turkey",
    "ISOCode": "TR"
  },
  {
    "name": "Turkmenistan",
    "ISOCode": "TM"
  },
  {
    "name": "Turks and Caicos Islands",
    "ISOCode": "TC"
  },
  {
    "name": "Tuvalu",
    "ISOCode": "TV"
  },
  {
    "name": "Uganda",
    "ISOCode": "UG"
  },
  {
    "name": "Ukraine",
    "ISOCode": "UA"
  },
  {
    "name": "United Arab Emirates",
    "ISOCode": "AE"
  },
  {
    "name": "United Kingdom of Great Britain and Northern Ireland",
    "ISOCode": "GB"
  },
  {
    "name": "United States Minor Outlying Islands",
    "ISOCode": "UM"
  },
  {
    "name": "United States of America",
    "ISOCode": "US"
  },
  {
    "name": "Uruguay",
    "ISOCode": "UY"
  },
  {
    "name": "Uzbekistan",
    "ISOCode": "UZ"
  },
  {
    "name": "Vanuatu",
    "ISOCode": "VU"
  },
  {
    "name": "Venezuela, Bolivarian Republic of",
    "ISOCode": "VE"
  },
  {
    "name": "Viet Nam",
    "ISOCode": "VN"
  },
  {
    "name": "Virgin Islands, British",
    "ISOCode": "VG"
  },
  {
    "name": "Virgin Islands, U.S.",
    "ISOCode": "VI"
  },
  {
    "name": "Wallis and Futuna",
    "ISOCode": "WF"
  },
  {
    "name": "Western Sahara",
    "ISOCode": "EH"
  },
  {
    "name": "Yemen",
    "ISOCode": "YE"
  },
  {
    "name": "Zambia",
    "ISOCode": "ZM"
  },
  {
    "name": "Zimbabwe",
    "ISOCode": "ZW"
  }
]