const nodemailer = require('nodemailer');

const sendEmail = async (options) => {
  // create transporter
  const transporter = nodemailer.createTransport({
    host: process.env.EMAIL_HOST,
    port: process.env.EMAIL_PORT,
    auth: {
      user: process.env.EMAIL_USERNAME,
      pass: process.env.EMAIL_PASSWORD,
    },
    secure: false,
  });

  // mail content
  const mail = {
    from: 'Rafiq Inc',
    to: options.email,
    subject: 'Password Reset [Rafiq]',
    text: `Hi, ${options.username}
    You requested to reset the password for your Rafiq account.
    Click the link below to reset your password.
    
    ${options.urlToken}
    
    This password reset is only valid for the next ${process.env.RESET_TOKEN_EXPIRES_IN} minutes.
    If you're having trouble clicking the link above, copy and paste the URL into your web browser directly.`,
    // html:
  };

  await transporter.sendMail(mail);
};

module.exports = sendEmail;
