const { STATUS_CODE } = require('./constants.js');

class ClientError extends Error {
  constructor(message, statusCode) {
    super(message);

    // needed for CustomError instanceof Error => true
    Object.setPrototypeOf(this, new.target.prototype);

    this.name = 'ClientError';
    this.message = message || 'something went very wrong';
    this.statusCode = statusCode || STATUS_CODE.InternalServerError;
    if (Error.captureStackTrace)
      Error.captureStackTrace(this, this.constructor);
  }
}

module.exports = ClientError;
