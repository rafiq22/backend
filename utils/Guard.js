const ClientError = require('./ClientError');
const catchAsync = require('./catchAsync');
const User = require('../models/users');

const self = function () {
    return catchAsync(async (req, res, next) => {
        if (req.user.userName === req.params.profileId) {
            return next();
        }
        next(new ClientError('You can not edit other users!', res.STATUS_CODE.Forbidden));
    });
};

const notSelf = function () {
    return catchAsync(async (req, res, next) => {
        if (req.user.userName === req.params.profileId) {
            return next(new ClientError('You can not make this for yourself', res.STATUS_CODE.Forbidden));
        }
        // make sure that req.params.profileId existing user
        if (!(await isExist(req.params.profileId))) {
            return next(new ClientError('This user does not exist!', res.STATUS_CODE.BadRequest));
        }
        if (await isBlockedBy(req.user.userName, req.params.profileId)) {
            return next(new ClientError('you are forbidden', res.STATUS_CODE.Forbidden));
        }
        if (await isBlockedBy(req.params.profileId, req.user.userName)) {
            return next(new ClientError('you are forbidden', res.STATUS_CODE.Forbidden));
        }
        next();
    });
};

const any = function () {
    return catchAsync(async (req, res, next) => {
        if (req.user.userName === req.params.profileId) {
            return next();
        }
        // make sure that req.params.profileId existing user
        if (!(await isExist(req.params.profileId))) {
            return next(new ClientError('This user does not exist!', res.STATUS_CODE.BadRequest));
        }
        if (await isBlockedBy(req.user.userName, req.params.profileId)) {
            return next(new ClientError('you are forbidden', res.STATUS_CODE.Forbidden));
        }
        if (await isBlockedBy(req.params.profileId, req.user.userName)) {
            return next(new ClientError('you are forbidden', res.STATUS_CODE.Forbidden));
        }

        next();
    });
};

const guard = {
    allow: {
        self,
        notSelf,
        any,
    }
}

module.exports = guard;


async function isExist(userName) {
    const user = await User.findOne({ userName: userName }, { userName: 1 });
    return !!user;
}

async function isBlockedBy(sink, source) {
    const result = await List.findOne({ _id: source, blockList: sink }, { _id: 1 });
    return !!result;
}