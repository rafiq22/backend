const cloudinary = require('cloudinary').v2;
const { CloudinaryStorage } = require('multer-storage-cloudinary');
// const ClientError = require('./ClientError');
// const STATUS_CODE = require('./constants').STATUS_CODE;
const dotenv = require('dotenv');
const path = require('path');

dotenv.config({ path: path.normalize(`${__dirname}/../.env`) });

cloudinary
  .config({
    cloud_name: process.env.CLOUDINARY_NAME,
    api_key: process.env.CLOUDINARY_API_KEY,
    api_secret: process.env.CLOUDINARY_API_SECRET,
    secure: true,
  })

const cld_storage = new CloudinaryStorage({
  cloudinary: cloudinary,
  params: {
    folder: async (req, file) => {
      return `${getFileType(file)}/${file.fieldname}s`;
      // todo make dynamic folders
    },
    format: async (req, file) => {
      if (getFileType(file) === 'image') return 'jpeg';
      else if (getFileType(file) === 'video') return 'mp4';
      throw new ClientError('Invalid file type!', STATUS_CODE.BadRequest);
    },
    public_id: async (req, file) => {
      if (getFileType(file) !== 'image' && getFileType(file) !== 'video') {
        throw new ClientError('Invalid file type!', STATUS_CODE.BadRequest);
      }
      return req.user.userName + '-' + Date.now() + '-' + (Math.random() + 1e-9) * 1e9;
    },
    resource_type: 'auto',
  },
});

module.exports = { cld: cloudinary, cld_storage };

function getFileType(file) {
  return file.mimetype.split('/')[0];
}