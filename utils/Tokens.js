const jwt = require('jsonwebtoken');
const catchAsync = require('./catchAsync');
const ClientError = require('./ClientError');

function getJwtTokenSecretKey(tokenType) {
  tokenType = tokenType.toLowerCase();
  let secret;
  if (tokenType === 'access') secret = process.env.AT_SECRET_KEY;
  else if (tokenType === 'refresh') secret = process.env.RT_SECRET_KEY;
  else if (tokenType === 'reset') secret = process.env.RESET_TOKEN_SECRET_KEY;
  else throw TypeError('Invalid tokenType value');
  return secret;
}

function getJwtTokenTimeToLive(tokenType) {
  tokenType = tokenType.toLowerCase();
  let expirationTime;
  if (tokenType === 'access') expirationTime = process.env.AT_EXPIRES_IN;
  else if (tokenType === 'refresh') expirationTime = process.env.RT_EXPIRES_IN;
  else if (tokenType === 'reset')
    expirationTime = process.env.RESET_TOKEN_EXPIRES_IN;
  else throw TypeError('Invalid tokenType value');
  return expirationTime;
}

function generateJwtToken(payload, tokenType) {
  const secret = getJwtTokenSecretKey(tokenType);
  const expirationTime = getJwtTokenTimeToLive(tokenType);
  return jwt.sign(payload, secret, {
    expiresIn: expirationTime,
  });
}

function verifyJwtToken(token, tokenType) {
  const secret = getJwtTokenSecretKey(tokenType);
  let res = {};
  jwt.verify(token, secret, (err, data) => {
    res = {
      // todo: analyse `err` and decide to return it or throw i
      // for example if err is about invalid or expired token then return it
      // but if it is about internal error or unexpected data then throw it
      error: err,
      user: data,
    };
  });
  return res;
}

module.exports.generateAccessToken = function (payload) {
  return generateJwtToken(payload, 'access');
};

module.exports.generateRefreshToken = function (payload) {
  return generateJwtToken(payload, 'refresh');
};

module.exports.generateResetToken = function (payload) {
  return generateJwtToken(payload, 'reset');
};

module.exports.verifyAccessToken = catchAsync(async (req, res, next) => {
  if (req.headers['access-token'] && req.cookies['access-token'] && req.headers['access-token'] !== req.cookies['access-token']) {
    return next(new ClientError('Received two non-equal tokens', res.STATUS_CODE.BadRequest));
  }

  req.headers['access-token'] = req.headers['access-token'] || req.cookies['access-token'];
  const accessToken = req.headers['access-token'];

  if (!accessToken) return next(new ClientError('Invalid/expired token', res.STATUS_CODE.Unauthorized));

  const { error, user } = verifyJwtToken(accessToken, 'access');
  if (error) return next(new ClientError('Invalid/expired token', res.STATUS_CODE.Unauthorized));
  req.user = user;
  next();
}
);

module.exports.verifyRefreshToken = catchAsync(async (req, res, next) => {
  if (req.headers['refresh-token'] && req.cookies['refresh-token'] && req.headers['refresh-token'] !== req.cookies['refresh-token']) {
    return next(new ClientError('Received two non-equal tokens', res.STATUS_CODE.BadRequest));
  }

  req.headers['refresh-token'] = req.headers['refresh-token'] || req.cookies['refresh-token'];
  const refreshToken = req.headers['refresh-token'];

  if (!refreshToken) return next(new ClientError('Please login', res.STATUS_CODE.Forbidden));

  const { error, user } = verifyJwtToken(refreshToken, 'refresh');
  if (error) return next(new ClientError('Please login', res.STATUS_CODE.Forbidden));
  req.user = user;
  next();
}
);


module.exports.verifyResetToken = catchAsync(async (req, res, next) => {
  const resetToken = req.headers['reset-token'];
  if (!resetToken) return next(new ClientError('Invalid/expired token', res.STATUS_CODE.BadRequest));
  const { error, user } = verifyJwtToken(resetToken, 'reset');
  if (error) return next(new ClientError('Invalid/expired token', res.STATUS_CODE.BadRequest));
  req.user = user;
  next();
}
);