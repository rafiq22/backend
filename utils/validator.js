const catchAsync = require('./catchAsync');
module.exports.validate = (schema) => {
  // return a middleware
  return catchAsync(async (req, res, next) => {
      await schema.validateAsync(req.body);
      next(); 
  });
};
