const Joi = require('joi');
const UserSchema = require('../models/users').schema.obj;
const { countryEnum } = require('./constants');
// todo set meaningful messages if Joi support this.

module.exports.register = Joi.object({
  firstName: Joi.string()
    .trim(true)
    .min(UserSchema.firstName.minlength)
    .max(UserSchema.firstName.maxlength)
    .required()
    .pattern(new RegExp(`^[a-z]{${UserSchema.firstName.minlength},${UserSchema.firstName.maxlength}}$`, "i")),
  lastName: Joi.string()
    .trim(true)
    .min(UserSchema.lastName.minlength)
    .max(UserSchema.lastName.maxlength)
    .required()
    .pattern(new RegExp(`^[a-z]{${UserSchema.lastName.minlength},${UserSchema.lastName.maxlength}}$`, "i")),
  userName: Joi.string()
    .trim(true)
    .min(UserSchema.userName.minlength)
    .max(UserSchema.userName.maxlength)
    .required()
    .custom((userName, helpers) => {
      if (userName.toLowerCase() === 'rafiq') return helpers.error('any.invalid');
      if (userName.length < UserSchema.userName.minlength) return helpers.error('any.invalid')
      if (userName.length > UserSchema.userName.maxlength) return helpers.error('any.invalid')

      const isAlpha = (str) => /^[a-z]+$/i.test(str);
      const isAlphaNum = (str) => /^([a-z]|\d)+$/i.test(str); // ensure this regex \d or \\d ?

      if (!isAlpha(userName[0])) return helpers.error('any.invalid');
      if (!isAlphaNum(userName[userName.length - 1])) return helpers.error('any.invalid');

      for (let i = 1; i < userName.length; ++i) {
        if (!isAlphaNum(userName[i]) && userName[i] !== '_') return helpers.error('any.invalid');
        if (userName[i] === '_' && userName[i - 1] === '_') return helpers.error('any.invalid');
      }
      return userName;
    }),
  email: Joi.string()
    .trim(true)
    .required()
    .min(UserSchema.email.minlength)
    .max(UserSchema.email.maxlength)
    .email({ tlds: { allow: ['com', 'net'] } }),
  password: Joi.string()
    .required()
    .min(UserSchema.password.minlength)
    .max(UserSchema.password.maxlength),
  confirmPassword: Joi
    .required()
    .valid(Joi.ref('password')),
  country: Joi.string()
    .required()
    .trim(true)
    .valid(...countryEnum),
  dateOfBirth: Joi.date()
    .required()
    .min(new Date().setFullYear(new Date().getFullYear() - 120))
    .max(new Date().setDate(new Date().getDate() - 7)),
  gender: Joi.string()
    .required()
    .trim(true)
    .insensitive()
    .valid('male', 'female'),
});


module.exports.login = Joi.object({
  emailOrUserName: Joi.string().required(),
  password: Joi.string().required(),
});


module.exports.resetPassword = Joi.object({
  password: Joi.string()
    .required()
    .min(UserSchema.password.minlength)
    .max(UserSchema.password.maxlength),
  confirmPassword: Joi
    .required()
    .valid(Joi.ref('password')),
});


module.exports.userInfo = Joi.object({
  firstName: Joi.string()
    .trim(true)
    .min(UserSchema.firstName.minlength)
    .max(UserSchema.firstName.maxlength)
    .pattern(new RegExp(`^[a-z]{${UserSchema.firstName.minlength},${UserSchema.firstName.maxlength}}$`, "i")),
  lastName: Joi.string()
    .trim(true)
    .min(UserSchema.lastName.minlength)
    .max(UserSchema.lastName.maxlength)
    .pattern(new RegExp(`^[a-z]{${UserSchema.lastName.minlength},${UserSchema.lastName.maxlength}}$`, "i")),
  country: Joi.string()
    .trim(true)
    .valid(...countryEnum),
  dateOfBirth: Joi.date()
    .min(new Date().setFullYear(new Date().getFullYear() - 120))
    .max(new Date().setDate(new Date().getDate() - 7)),
  gender: Joi.string()
    .trim(true)
    .insensitive()
    .valid('male', 'female'),
  liveIn: Joi.string()
    .trim(true)
    .valid(...countryEnum),
  socialMedia: Joi.array()
    .max(10).items(Joi.object({
      userName: Joi.string().required()
        .pattern(new RegExp('^[^/?]+$')),
      label: Joi.string().required()
        .trim(true)
        .insensitive()
        .valid('Facebook', 'Instagram', 'Youtube', 'Tiktok'),
    })).unique((a, b) => a.userName === b.userName && a.label === b.label),
});

module.exports.travelMap = Joi.object({
  type: Joi.string()
    .required().insensitive().valid('done', 'wish'),
  latitude: Joi.number().required(),
  longitude: Joi.number().required(),
});