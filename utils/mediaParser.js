const multer = require('multer');
const ClientError = require('./ClientError');
const STATUS_CODE = require('./constants').STATUS_CODE;
const catchAsync = require('./catchAsync');
const { cld_storage } = require('./cloudinary');

const pictureParser = multer({
    // note you can't know the file size in fileFilter function
    // so we will handle the minium size of file in the next middleware
    storage: cld_storage,
    fileFilter: function (req, file, cb) {
        if (file.mimetype.startsWith('image/')) {
            return cb(null, true);
        }
        return cb(new ClientError('there are file type that is not allowed', STATUS_CODE.BadRequest))
    },
    limits: {
        // the max file size (in bytes)
        fileSize: 3 * 1024 * 1024, // 3 MB
        files: 15, // maximum number of file fields
    }
});

const picturesOrVideoParser = multer({
    // note you can't know the file size in fileFilter function
    // so we will handle the minimum size of file in the middleware in the controller
    storage: cld_storage,
    fileFilter: function (req, file, cb) {
        if (file.mimetype.startsWith('image/')) {
            return cb(null, true);
        }
        if (file.mimetype.startsWith('video/')) {
            return cb(null, true);
        }
        return cb(new ClientError('there are file type that is not allowed', STATUS_CODE.BadRequest))
    },
    limits: {
        // the max file size (in bytes)
        fileSize: 20 * 1024 * 1024, // 20 MB
        files: 15, // maximum number of file fields
    }
});



const deleteFiles = async (files) => {
    for (let j = 0; j < files.length; ++j) {
        cld.uploader.destroy(files[j].filename);
    }
}

const filterBySize = async (files) => {
    for (let i = 0; i < files.length; ++i) {
        if (files[i].size >= 15 * 1024) continue;
        deleteFiles(files);
        throw new ClientError('too small image, please upload at least a 15KB image', STATUS_CODE.BadRequest);
    }
}


const pictureFilter = catchAsync(async (req, res, next) => {
    await filterBySize(req.files);
    next();
});

const picturesOrVideoFilter = catchAsync(async (req, res, next) => {
    let pictureFound = false, videoFound = false;
    for (let i = 0; i < req.files.length; ++i) {
        if (req.files[i].mimetype.startsWith('image/')) {
            pictureFound = true;
        }
        else if (req.files[i].mimetype.startsWith('video/')) {
            videoFound = true;
        }
    }

    if (pictureFound && videoFound) {
        deleteFiles(req.files);
        return next(new ClientError('you can upload pictures or one video but not mixed', res.STATUS_CODE.BadRequest));
    }

    if (videoFound && req.files.length > 1) {
        deleteFiles(req.files);
        return next(new ClientError('you can only upload one video', res.STATUS_CODE.BadRequest));
    }

    await filterBySize(req.files);
    next();
});

const multerParser = {
    picture: function (fieldName, maxCount = 1) {
        return [pictureParser.array(fieldName, maxCount), pictureFilter];
    },
    picturesOrVideo: function (fieldName, maxCount = 15) {
        return [picturesOrVideoParser.array(fieldName, maxCount), picturesOrVideoFilter];
    },

    pictureParserToFileSystem: function (fieldName, maxCount, dest) {
        const uploader = multer({
            dest: dest,
            fileFilter: function (req, file, cb) {
                if (!file.mimetype.startsWith('image/')) {
                    return cb(new ClientError('You can only upload image files', STATUS_CODE.BadRequest))
                }
                cb(null, true);
            },
            limits: {
                // the max file size (in bytes)
                fileSize: 3 * 1024 * 1024, // 3 MB
                files: 1, // maximum number of file fields
            }
        });
        return uploader.array(fieldName, maxCount);
    }
}

module.exports = multerParser;

