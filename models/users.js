const mongoose = require('mongoose');
const crypto = require('crypto');
const bcrypt = require('bcryptjs');
const ClientError = require('../utils/ClientError');
const { countryEnum, STATUS_CODE } = require('../utils/constants');

const socialMediaSchema = require('../subSchemas/socialMedia');// todo


// todo: set meaningful messages
const userSchema = new mongoose.Schema({
  firstName: {
    type: String,
    minlength: 2,
    maxlength: 35,
    trim: true,
    required: true,
  },
  lastName: {
    type: String,
    minlength: 2,
    maxlength: 35,
    trim: true,
    required: true,
  },
  userName: {
    type: String,
    minlength: 3,
    maxlength: 35,
    lowercase: true,
    trim: true,
    required: true,
    unique: true,
    index: true
  },
  email: {
    type: String,
    minlength: 5,
    maxlength: 255,
    required: true,
    lowercase: true,
    trim: true,
    unique: true,
    // todo add validator to check if it's email
  },
  password: {
    type: String,
    minlength: 8,
    maxlength: 64,
    required: true,
  },
  country: { // country user was born in
    type: String,
    enum: [...countryEnum],
    trim: true,
    required: true,
  },
  dateOfBirth: {
    type: Date,
    required: true,
    min: new Date().setFullYear(new Date().getFullYear() - 120),
    max: new Date().setDate(new Date().getDate() - 7),
  },
  gender: {
    type: String,
    trim: true,
    lowercase: true,
    enum: ['male', 'female'],
    required: true,
  },
  cover: {
    type: String,
    default: null,
  },
  avatar: {
    type: String,
    default: null,
  },
  numberOfFollowers: {
    type: Number,
    default: 0,
    min: 0,
  },
  numberOfFollowings: {
    type: Number,
    default: 0,
    min: 0,
  },
  resetToken: {
    type: String,
    trim: true,
    minlength: 1,
    default: function () {
      return crypto.randomBytes(10).toString('hex');
    }
  },
  liveIn: {
    type: String,
    enum: [...countryEnum],
    trim: true,
  },
  confirmed: {
    type: Boolean,
    default: true,
    // process.env.NODE_ENV !== 'production', // todo make it false
  },
  socialMedia: {
    type: [socialMediaSchema],
    maxlength: 10,
  },
  posts: [],
  newsFeed: [],
});

userSchema.pre('save', async function () {
  // hash password
  this.password = await hashPassword(this.password);
  this._id = this.userName;
});

userSchema.statics.updatePasswordByUserName = async function (newPassword, userName) {
  await this.updateOne({ userName: userName }, { $set: { password: await hashPassword(newPassword) } });
}

userSchema.statics.incrementNumberOfFollowers = async function (userName) {
  const updateRes = await this.updateOne({ userName: userName }, { $inc: { numberOfFollowers: 1 } });
  if (!updateRes.modifiedCount) {
    throw new ClientError('The user you want to follow does not exist!', STATUS_CODE.BadRequest);
  }
}


userSchema.statics.incrementNumberOfFollowings = async function (userName) {
  const updateRes = await this.updateOne({ userName: userName }, { $inc: { numberOfFollowings: 1 } });
  if (!updateRes.modifiedCount) {// no error since the user already passed auth
    throw new ClientError('undefined', STATUS_CODE.BadRequest);
  }
}

userSchema.statics.decrementNumberOfFollowers = async function (userName) {
  const updateRes = await this.updateOne({ userName: userName }, { $inc: { numberOfFollowers: -1 } });
  if (!updateRes.modifiedCount) {
    throw new ClientError('The user you want to unfollow does not exist!', STATUS_CODE.BadRequest);
  }
}


userSchema.statics.decrementNumberOfFollowings = async function (userName) {
  const updateRes = await this.updateOne({ userName: userName }, { $inc: { numberOfFollowings: -1 } });
  if (!updateRes.modifiedCount) {// no error since the user already passed auth
    throw new ClientError('undefined', STATUS_CODE.BadRequest);
  }
}

userSchema.statics.addPost = async function (author, postId) {
  const updateResult = await this.updateOne({ userName: author }, { $push: { posts: postId } });
  if (!updateResult.modifiedCount) {
    throw new ClientError('Could not upload Post', STATUS_CODE.BadRequest);
  }
}

userSchema.statics.addPostToNewsFeed = async function (author, post) {

  const updateResult = await this.updateOne({ userName: author }, { $push: { newsFeed: post } });
  if (!updateResult.modifiedCount) {
    throw new ClientError('Could not upload Post', STATUS_CODE.BadRequest);
  }
}

userSchema.statics.getNewsFeed = async function (userName) {
  try {
    return await this.findOne({ userName: userName }, { _id: 0, newsFeed: 1 });
  } catch (err) {
    throw new ClientError('Could Not Find This User NewsFeed', STATUS_CODE.BadRequest);
  }
}


module.exports = User = mongoose.model('users', userSchema);


async function hashPassword(password) {
  return await bcrypt.hash(password, 12);
}