const mongoose = require('mongoose');

const User = require('./users');
const Followings = require('./followings');
// const Picture = require('./picture');
// const Video = require('./video');
const ClientError = require('../utils/ClientError');
const STATUS_CODE = require('../utils/constants');

const UserSchema = User.schema.obj;
const userNameProperties = { ...UserSchema.userName };
const firstNameProperties = {...UserSchema.firstName};
const lastNameProperties = {...UserSchema.lastName};
const avatarProperties = {...UserSchema.avatar};

delete userNameProperties.unique;
delete userNameProperties.index;



const contentSchema = require('../subSchemas/content');
const sharedSourceSchema = require('../subSchemas/sharedSource');

const postSchema = new mongoose.Schema({
    authorInfo: {
        userName: { ...userNameProperties },
        firstName: { ...firstNameProperties },
        lastName: { ...lastNameProperties },
        avatar: { ...avatarProperties }, 
    },
    content: {
        type: contentSchema,
        required: true,
    },
    isLiked: {
        type: Boolean,
        default: false,
    },
    numberOfLikes: {
        type: Number,
        default: 0,
        min: 0,
    },
    numberOfComments: {
        type: Number,
        default: 0,
        min: 0,
    },
    shares: [],
    isShared: {
        type: Boolean,
        default: false,
        required: true,
    },
    sharedSource: {
        type: sharedSourceSchema,
        default: {
            authorInfo: {
                userName: null,
                firstName: null,
                lastName: null,
                avatar: null,
            },
            postId: null,
        }
    }
});

postSchema.index({'authorInfo.userName': 1, _id: 1});

// postSchema.statics.addContent = async function (author, rawContent){
//     for(const content of rawContent){
//         if(content.slice(-3) === 'jpg'){
//             await Picture.add(author, content);
//         }
//         else {
//             await Video.add(author, content);
//         }
//     }
// }

postSchema.statics.createPost = async function (author, content) {
    // todo: i think aggregation is better
    // because this logic will be done on db server
    const authorInfo = await User.findOne( {userName: author }, { _id:0, userName:1, firstName:1, lastName:1, avatar:1 } );
    
    // await this.addContent(author, content);
    const post = new this({ authorInfo, content });
    await User.addPost(author, post.id);

    const followers = await Followings.getFollowers(author);
    for (const follower of followers) {
        await User.addPostToNewsFeed(follower, post)
    }
    await post.save();
}

postSchema.statics.getPost = async function (postId) {
    try {
        return await this.findById(postId);
    } catch (err) {
        throw new ClientError('Could Not Find This Post', STATUS_CODE.BadRequest);
    }
}

postSchema.statics.deletePost = async function (postId) {
    const deleteResult = await this.deleteOne({ _id: postId });
    if (!deleteResult.deletedCount)
        throw new ClientError('This Post Does Not Exist', STATUS_CODE.BadRequest);

    return true;
}

postSchema.statics.incrementNumberOfLikes = async function (postId) {
    const updateResult = await this.updateOne({ _id: postId }, { $inc: { numberOfLikes: 1 } });
    if (!updateResult.modifiedCount) {
        throw new ClientError('Could Not Find This Post', STATUS_CODE.BadRequest);
    }
}

postSchema.statics.decrementNumberOfLikes = async function (postId) {
    const updateResult = await this.updateOne({ _id: postId }, { $inc: { numberOfLikes: -1 } });
    if (!updateResult.modifiedCount) {
        throw new ClientError('Could Not Find This Post', STATUS_CODE.BadRequest);
    }
}


postSchema.statics.sharePost = async function (author, content, postId) {
    const originalPost = await this.findOne({ _id: postId }, { _id: 0, authorInfo: 1 });
   
    const sharedSource = {
        authorInfo: originalPost.authorInfo,
        postId: postId,
    }
    const authorInfo = await User.findOne( {userName: author }, { _id:0, userName:1, firstName:1, lastName:1, avatar:1 } );
    const post = new this({ authorInfo, content, sharedSource, isShared: true });

    await User.addPost(author, post.id);
    await this.updateOne({ _id: postId }, { $push: { shares: author } });

    const followers = await Followings.getFollowers(author);
    for (const follower of followers) {
        await User.addPostToNewsFeed(follower, post.id)
    }
    await post.save();
}

postSchema.statics.incrementNumberOfComments = async function (postId) {
    const updateResult = await this.updateOne({ _id: postId }, { $inc: { numberOfComments: 1 } });
    if (!updateResult.modifiedCount) {
        throw new ClientError('Could Not Find This Post', STATUS_CODE.BadRequest);
    }
}

postSchema.statics.decrementNumberOfComments = async function (postId) {
    const updateResult = await this.updateOne({ _id: postId }, { $inc: { numberOfComments: -1 } });
    if (!updateResult.modifiedCount) {
        throw new ClientError('Could Not Find This Post', STATUS_CODE.BadRequest);
    }
}

postSchema.statics.getPosts = async function (author) {
    try {
        return await this.find( {'authorInfo.userName': author}).limit(10);
    } catch (err) {
        throw new ClientError(`Could Not get ${author} posts`, STATUS_CODE.BadRequest);
    }
}

postSchema.statics.getMorePosts = async function (author, lastPostId) {
    try {
        return await this.find( {'authorInfo.userName': author, _id: {$gt: lastPostId}}).limit(10);
    } catch (err) {
        throw new ClientError(`Could Not get ${author} posts`, STATUS_CODE.BadRequest);
    }
}





module.exports = Posts = mongoose.model('Posts', postSchema);