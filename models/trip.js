const mongoose = require('mongoose');

const User = require('./users');
const ClientError = require('../utils/ClientError');
const STATUS_CODE = require('../utils/constants');
const City = require('./city');
const Country = require('./country');
const location = require('../subSchemas/location');

const UserSchema = User.schema.obj;
const userNameProperties = { ...UserSchema.userName };
const firstNameProperties = { ...UserSchema.firstName };
const lastNameProperties = { ...UserSchema.lastName };
const avatarProperties = { ...UserSchema.avatar };

delete userNameProperties.unique;
delete userNameProperties.index;

const citySchema = City.schema.obj;
const countrySchema = Country.schema.obj;


const tripSchema = new mongoose.Schema({
    userName: { ...userNameProperties },

    images: [],
    country: {
        type: [],
    },
    city: {
        type: [],
    },
    description: {
        type: String,
        default: "",
        maxlength: 1024,
    },
    totalPrice: {
        type: Number,
        default: 0,
        min: 0,
    },
    rating: {
        type: Number,
        default: 0,
        min: 0,
    },
    locations: {
        type: [],
    }

});


tripSchema.statics.createTrip = async function (tripInfo, tripImages) {
    let trip = new this(tripInfo);
    trip.images = tripImages;
    await trip.save();
}

tripSchema.statics.findBestRated = async function () {
    try {
        return await this.find({}).sort({ rating: -1 });
    } catch (err) {
        throw new ClientError(`Could Not get trips`, STATUS_CODE.BadRequest);
    }

}

tripSchema.statics.findHighestPrice = async function () {
    try {
        return await this.find({}).sort({ totalPrice: -1 });
    } catch (err) {
        throw new ClientError(`Could Not get trips`, STATUS_CODE.BadRequest);
    }

}
tripSchema.statics.findLowestPrice = async function () {
    try {
        return await this.find({}).sort({ totalPrice: 1 });
    } catch (err) {
        throw new ClientError(`Could Not get trips`, STATUS_CODE.BadRequest);
    }

}

tripSchema.statics.findById = async function (tripId) {
    try {
        return await this.find({ _id: tripId });
    } catch (err) {
        throw new ClientError(`Could Not get trip`, STATUS_CODE.BadRequest);
    }

}







module.exports = Trip = mongoose.model('Trips', tripSchema);