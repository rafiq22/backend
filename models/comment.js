const mongoose = require('mongoose');

const ClientError = require('../utils/ClientError.js');
const STATUS_CODE = require('../utils/constants');

const Post = require('../models/posts');

const UserSchema = User.schema.obj;
const userNameProperties = { ...UserSchema.userName };
delete userNameProperties.unique;
delete userNameProperties.index;

const contentSchema = require('../subSchemas/content');

const commentSchema = new mongoose.Schema({
    author: {
        ...userNameProperties,
    },
    content: {
        type: contentSchema,
    },
    isLiked: {
        type: Boolean,
        default: false,
    },
    numberOfLikes: {
        type: Number,
        default: 0,
        min: 0,
    },
    postId: {
        type: String,
        default: "",
        required: true,
        index: true,
    }
});

commentSchema.index({ postId: 1, _id: 1 });

commentSchema.statics.addComment = async function (author, postId, text) {
    await Post.incrementNumberOfComments(postId);
    const relation = await new this({ author: author, postId: postId, content: { text } });
    await relation.save();
    return relation._id.toString();
}

commentSchema.statics.updatedComment = async function (commentId, text) {
    const updateResult = await this.updateOne({ _id: commentId }, { 'content.text': text });
    if (!updateResult.modifiedCount) {
        throw new ClientError('Could Not update This comment', STATUS_CODE.BadRequest);
    }
}

commentSchema.statics.deleteComment = async function (commentId, postId) {
    await Post.decrementNumberOfComments(postId);
    const deleteResult = await this.deleteOne({ _id: commentId });
    if (!deleteResult.deletedCount) {
        throw new ClientError(`Could not delete this comment`, STATUS_CODE.BadRequest);
    }
}

commentSchema.statics.getComments = async function (postId) {
    try {
        return await this.find({ postId: postId }, { postId: 0 }).limit(10);
    }
    catch (err) {
        throw new ClientError(`Could not get this post ${postId} comments`, STATUS_CODE.BadRequest);
    }
}

commentSchema.statics.getMoreComments = async function (postId, lastCommentId) {
    try {
        return await this.find({ postId: postId, _id: { $gt: lastCommentId } }, { postId: 0 }).limit(10);
    }
    catch (err) {
        throw new ClientError(`Could not get this post ${postId} comments`, STATUS_CODE.BadRequest);
    }
}

commentSchema.statics.getNumberOfComments = async function (postId) {
    const { numberOfComments } = await Post.findOne({ _id: postId }, { numberOfComments: 1, _id: 0 });
    return numberOfComments;
}

commentSchema.statics.incrementNumberOfLikes = async function (postId) {
    const updateResult = await this.updateOne({ _id: postId }, { $inc: { numberOfLikes: 1 } });
    if (!updateResult.modifiedCount) {
        throw new ClientError('Could Not Find This Post', STATUS_CODE.BadRequest);
    }
}

commentSchema.statics.decrementNumberOfLikes = async function (postId) {
    const updateResult = await this.updateOne({ _id: postId }, { $inc: { numberOfLikes: -1 } });
    if (!updateResult.modifiedCount) {
        throw new ClientError('Could Not Find This Post', STATUS_CODE.BadRequest);
    }
}



module.exports = Comment = mongoose.model('comments', commentSchema);
