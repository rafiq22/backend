const mongoose = require('mongoose');

const travelMapSchema = require('../subSchemas/travelMap');

const listSchema = new mongoose.Schema({
    _id: { // relation id
        type: String,
        required: true,
        alias: 'userName',
    },
    blockList: {
        type: [String],
        required: true,
        default: [],
    },
    travelMap: {
        type: [travelMapSchema],
        default: [],
    }
});

module.exports = List = mongoose.model('lists', listSchema);
