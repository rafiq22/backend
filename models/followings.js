const mongoose = require('mongoose');

const ClientError = require('../utils/ClientError.js');
const { STATUS_CODE } = require('../utils/constants');
const User = require('./users');

const UserSchema = User.schema.obj;
const userNameProperties = { ...UserSchema.userName };
delete userNameProperties.unique;
delete userNameProperties.index;

// note: [source] (user who follows another user) --> [sink] (famous user who is followed)
const followingSchema = new mongoose.Schema({
    _id: { // relation id
        type: String,
        alias: 'relationId'
    },
    source: {
        ...userNameProperties,
    },
    sink: {
        ...userNameProperties,
        alias: 'destination',
        index: true,
    }
});

followingSchema.pre('save', async function () {
    this._id = `${this.source}>${this.sink}`
});

followingSchema.statics.isFollowing = async function (userId, profileId) {
    const result = await this.findOne({ _id: `${userId}>${profileId}` });
    return result ? true : false;
};


followingSchema.statics.follow = async function (userId, profileId, profileType) {
    if (await this.isFollowing(userId, profileId)) {
        throw new ClientError(`You're already following ${profileId}!`, STATUS_CODE.BadRequest)
    }

    await User.incrementNumberOfFollowings(userId);
    await mongoose.model(profileType).incrementNumberOfFollowers(profileId);

    const relation = await new this({ source: userId, sink: profileId });
    await relation.save();
}

followingSchema.statics.unFollow = async function (userId, profileId, profileType) {
    if (!(await this.isFollowing(userId, profileId))) {
        throw new ClientError(`You already do not follow ${profileId}!`, STATUS_CODE.BadRequest)
    }

    await User.decrementNumberOfFollowings(userId);
    await mongoose.model(profileType).decrementNumberOfFollowers(profileId);

    await this.deleteOne({ _id: `${userId}>${profileId}` });
}


followingSchema.statics.getFollowers = async function (user) {
    let followers = await this.find({ sink: user }, { source: 1, _id: 0 });
    // TODO use aggregation to do the work on mongo server side
    await followers.forEach((user, index) => followers[index] = user['source']);
    return followers;
}

followingSchema.statics.getFollowings = async function (user) {
    let followings = await this.find({ _id: /^user>*/ }, { sink: 1, _id: 0 });
    // TODO use aggregation to do the work on mongo server side
    await followings.forEach((user, index) => followings[index] = user['sink']);

    return followings;
}

// followingSchema.statics.getNumberOfFollowers = async function (user) {
//     const { numberOfFollowers } = await User.findOne({ userName: user }, { numberOfFollowers: 1, _id: 0 });
//     return numberOfFollowers;
// }

// followingSchema.statics.getNumberOfFollowings = async function (user) {
//     const { numberOfFollowings } = await User.findOne({ userName: user }, { numberOfFollowings: 1, _id: 0 });
//     return numberOfFollowings;
// }


module.exports = Following = mongoose.model('followings', followingSchema);
