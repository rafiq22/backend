const mongoose = require('mongoose');

const ClientError = require('../utils/ClientError.js');
const STATUS_CODE = require('../utils/constants');

const Post = require('../models/posts');
const Comment = require('../models/comment');

const UserSchema = User.schema.obj;
const userNameProperties = { ...UserSchema.userName };
delete userNameProperties.unique;
delete userNameProperties.index;

const likeSchema = new mongoose.Schema({
    _id: {
        type: String,
        alias: 'likeId'
    },
    user: {
        ...userNameProperties,
    },
    modelId: {
        type: String,
        default: "",
        required: true,
        index: true,
    }
});

likeSchema.pre('save', async function () {
    this._id = `${this.user}>${this.modelId}`
});

likeSchema.statics.isLiked = async function (user, modelId) {
    const result = await this.findOne({ _id: `${user}>${modelId}` });
    return result ? true : false;
};
likeSchema.statics.like = async function (user, modelId, Model) {
    const liked = await this.isLiked(user, modelId);
    if (!liked) {
        await Model.incrementNumberOfLikes(modelId);
        const relation = await new this({ user: user, modelId: modelId });
        await relation.save();
    }
}

likeSchema.statics.unLike = async function (user, modelId, Model) {
    const liked = await this.isLiked(user, modelId);
    if (liked) {
        await Model.decrementNumberOfLikes(modelId);
        const deleteResult = await this.deleteOne({ _id: `${user}>${modelId}` });
        if (!deleteResult.deletedCount) {
            throw new ClientError(`Could not un-like ${modelId}`, STATUS_CODE.BadRequest);
        }
    }
}

likeSchema.statics.getUsersWhoLiked = async function (modelId) {
    let likes = await this.find({ modelId: modelId }, { user: 1, _id: 0 });
    // TODO use aggregation to do the work on mongo server side
    await likes.forEach((like, index) => likes[index] = like['user']);
    return likes;
}

likeSchema.statics.getNumberOfLikes = async function (modelId, Model) {
    const { numberOfLikes } = await Model.findOne({ _id: modelId }, { numberOfLikes: 1, _id: 0 });
    return numberOfLikes;
}



module.exports = Like = mongoose.model('likes', likeSchema);
