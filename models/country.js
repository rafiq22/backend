const mongoose = require('mongoose');
const emergencyCallsSchema = require('../subSchemas/emergencyCalls');

// todo get data from here:
// https://gist.github.com/keeguon/2310008

const countrySchema = mongoose.Schema({
    _id: {
        type: String,
        minLength: 2,
        maxLength: 2,
        alias: "ISOCode",
    },
    ISONumeric: {
        type: String,
        default: null,
    },
    name: {
        type: String,
        require: true,
    },
    flag: {
        type: String,
        default: null,
    },
    capital: {
        type: String,
        default: null,
    },
    language: {
        type: {
            code: String,
            name: String,
        },
        default: null,
    },
    currency: {
        type: {
            code: String,
            name: String,
            symbol: String,
        },
        default: null,
    },
    region: {
        type: String,
        default: null,
    },
    emergencyNumbers: {
        type: emergencyCallsSchema,
        default: null,
    },
});


module.exports = Country = mongoose.model('countries', countrySchema);
