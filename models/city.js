const mongoose = require('mongoose');

const citySchema = mongoose.Schema({
    _id: {
        type: String,
    },
    // note: this is actually the full name 
    // but is called firstName and there is also lastName
    // just for the compatibility with User Schema
    firstName: {
        type: String,
        require: true,
    },
    lastName: {
        type: String,
        require: true,
        default: "",
    },
    ascii_name: {
        type: String,
        default: null,
    },
    native_name: {
        type: String,
        default: null,
    },
    images: {
        type: [String],
        default: null,
    },
    location: {
        latitude: Number,
        longitude: Number,
    },
    timeZone: {
        type: String,
        default: null,
    },
    country: {
        type: String,
    },
    admins: {
        type: [String,],
        default: null,
    },
    population: {
        type: Number,
        default: null,
    },
    numberOfFollowers: {
        type: Number,
        default: 0,
        min: 0,
    },
    admins: {
        type: [String],
    }
});

citySchema.statics.incrementNumberOfFollowers = async function (cityId) {
    await this.updateOne({ _id: cityId }, { $inc: { numberOfFollowers: 1 } });
}


citySchema.statics.decrementNumberOfFollowers = async function (cityId) {
    await this.updateOne({ _id: cityId }, { $inc: { numberOfFollowers: -1 } });
}

module.exports = City = mongoose.model('cities', citySchema);