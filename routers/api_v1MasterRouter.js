const router = require('express').Router();

const authRouter = require('./api_v1/auth');
const newsFeedRouter = require('./api_v1/newsFeed');
const userProfileRouter = require('./api_v1/users');
const cityProfileRouter = require('./api_v1/city');
const generalProfileRouter = require('./api_v1/profile');

const countryRouter = require('./api_v1/country');
const searchRouter = require('./api_v1/search');
const Tokens = require('../utils/Tokens');

const tripRouter = require('./api_v1/trip');

const guard = require('../utils/Guard');

router.use('/auth', authRouter);
router.use(Tokens.verifyAccessToken); // authentication
router.use('/newsFeed', newsFeedRouter);
// router.use('/users/:userId', userRouter);
// router.use('/cities/:cityId', cityRouter);
router.use('/countries/:countryId', countryRouter);
router.use('/search', searchRouter);

router.use('/trips', tripRouter);

router.use('/:profileType(cities|users)?/:profileId', (req, res, next) => {
    const profileId = req.params.profileId;
    if (!req.params.profileType) {
        if ('' + +profileId === profileId) req.params.profileType = 'cities';
        else req.params.profileType = 'users';
    }
    next();
}, generalProfileRouter);

router.use('/:profileType(cities)/:profileId', (req, res, next) => {
    if (!req.params.profileType) req.params.profileType = "cities";
    next();
}, cityProfileRouter);

router.use('/:profileType(users)/:profileId', (req, res, next) => {
    if (!req.params.profileType) req.params.profileType = "users";
    next();
}, userProfileRouter);

module.exports = router;
