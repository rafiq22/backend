const router = require('express').Router();
const newsFeedController = require('../../controllers/newsFeed.js');

router //
  .route('/')
  .get(newsFeedController.get);

module.exports = router;
