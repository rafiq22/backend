const router = require('express').Router({ mergeParams: true });

const searchController = require('../../controllers/search');
const multerParser = require('../../utils/mediaParser');
const path = require('path');

router
    .route('/liveSearch')
    .get(searchController.getSuggestedUsers, searchController.getSuggestedCities);

router
    .route('/image')
    .post(multerParser.pictureParserToFileSystem('query', 1, path.normalize(`${__dirname}/../../CBIR/queries`)),
        searchController.searchByImage);

module.exports = router;