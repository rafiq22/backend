const router = require('express').Router({ mergeParams: true });
const listController = require('../../controllers/lists.js');

const validate = require('../../utils/validator').validate;
const JoiSchemas = require('../../utils/JoiSchemas');

const guard = require('../../utils/Guard');

router //
  .route('/blockList')
  .get(guard.allow.self(), listController.getBlockList);

router //
  .route('/travelMap')
  .get(listController.getUserTravelMap)
  .all(guard.allow.self())
  .post(validate(JoiSchemas.travelMap), listController.addMarkerToTravelMap)
  .delete(listController.deleteMarkerFromTravelMap);



module.exports = router;
