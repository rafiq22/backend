const router = require('express').Router({ mergeParams: true });
const postRouter = require('./posts');
const followingController = require('../../controllers/following');

const guard = require('../../utils/Guard');

router.use('/posts', postRouter);


router
    .route('/isFollowed')
    .get(followingController.isFollowed);

router
    .route('/follow')
    .post(/*guard.allow.notSelf(),*/ followingController.follow);

router
    .route('/unFollow')
    .post(/*guard.allow.notSelf(),*/ followingController.unfollow);

// router
//     .route('/getFollowers')
//     .get(followingController.getFollowers);

// router
//     .route('/getFollowings')
//     .get(followingController.getFollowings);

// router
//     .route('/getNumberOfFollowers')
//     .get(followingController.getNumberOfFollowers);

// router
//     .route('/getNumberOfFollowings')
//     .get(followingController.getNumberOfFollowings);

// router
//     .route('/getNumberOfFollowers')
//     .get(followingController.getNumberOfFollowers);

// router
//     .route('/getNumberOfFollowings')
//     .get(followingController.getNumberOfFollowings);



module.exports = router;



