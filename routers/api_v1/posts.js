const router = require('express').Router({ mergeParams: true });
const postController = require('../../controllers/posts.js');
const commentRouter = require('./comments');

const mediaParser = require('../../utils/mediaParser');

const guard = require('../../utils/Guard');

router //
  .route('/')
  .post(guard.allow.self(), mediaParser.picturesOrVideo('post'), postController.create)
  .get(postController.getSomeUserPosts);

router //
  .get('/images', postController.getOnlyImages)
  .get('/videos', postController.getOnlyVideos);

router
  .route('/morePosts/:lastPostId')
  .get(postController.getMoreUserPosts);

router //
  .route('/:postId')
  .get(postController.get) // get post by id
  .all(guard.allow.self())
  .put(postController.update) // update post content
  .delete(postController.delete); // delete post

router //
  .put('/:postId/like', postController.like)
  .delete('/:postId/like', postController.unLike)
  .get('/:postId/isLiked', postController.isLiked)
  .get('/:postId/usersWhoLiked', postController.getUsersWhoLiked)
  .get('/:postId/numberOfLikes', postController.getNumberOfLikes)
  .post('/:postId/views', postController.incrementViews)
  .post('/:postId/share', guard.allow.self(), mediaParser.picturesOrVideo('post'), postController.share);


router //
  .delete('/:postId/media/:mediaId', guard.allow.self(), postController.deleteMedia);

// comments routing
router.use('/:postId/comments', commentRouter);

module.exports = router;
