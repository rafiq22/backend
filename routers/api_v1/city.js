const router = require('express').Router({ mergeParams: true });
const postRouter = require('./posts');

const cityController = require('../../controllers/city');

router
    .route('/')
    .get(cityController.getCityInfo);

router
    .get('/activities', cityController.getCityActivities)
    .get('/hotels', cityController.getCityHotels);

router.use('/posts', postRouter);

module.exports = router;