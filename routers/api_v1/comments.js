const router = require('express').Router({ mergeParams: true });
const commentController = require('../../controllers/comments.js');

const guard = require('../../utils/Guard');

// router //
//   .route('/')
//   .post(commentController.create) // to go this you must be (owner) of the post or (has permission)
//   .get(commentController.get); // owner of post or has permission

// router //
//   .route('/:commentId')
//   .put(commentController.update) // owner of comment
//   .delete(commentController.delete); // owner of comment or (owner of post ???)

// router //
//   .put('/:commentId/like', commentController.like)
//   .delete('/:commentId/like', commentController.unlike);

router
  .route('/')
  .post(commentController.create)
  .get(commentController.get); // get comments of the post

router
  .route('/moreComments/:lastCommentId')
  .get(commentController.getMoreComments);

router
  .route('/:commentId')
  // fixme this allows the owner of post to edit
  // the comment not the actual owner of it.
  // .all(guard.allow.self()) // todo: add authorization
  .put(commentController.update)
  .delete(commentController.delete);

router
  .route('/:commentId/like')
  .get(commentController.getNumberOfLikes)
  .put(commentController.like)
  .delete(commentController.unLike);

router
  .get('/:commentId/usersWhoLiked', commentController.getUsersWhoLiked)

module.exports = router;
