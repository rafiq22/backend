const router = require('express').Router();
const authController = require('../../controllers/auth.js');
const validate = require('../../utils/validator').validate;
const JoiSchemas = require('../../utils/JoiSchemas');
const Tokens = require('../../utils/Tokens');

router //
  .route('/register')
  .post(validate(JoiSchemas.register), authController.register);

router //
  .route('/login')
  .post(validate(JoiSchemas.login), authController.login);

router //
  .route('/forgotPassword')
  .post(authController.forgotPassword);

router //
  .route('/resetPassword')
  .put(Tokens.verifyResetToken, validate(JoiSchemas.resetPassword), authController.resetPassword);

router //
  .route('/accessToken')
  .post(Tokens.verifyRefreshToken, authController.getAccessToken);

module.exports = router;
