const router = require('express').Router({ mergeParams: true });
const tripController = require('../../controllers/trip.js');

const mediaParser = require('../../utils/mediaParser');
const guard = require('../../utils/Guard');

router //
  .route('/')
  .post(mediaParser.picture('tripImages', 15), tripController.create)
  .get(tripController.getFilter)

router
  .route('/:tripId')
  .get(tripController.getById)


module.exports = router;