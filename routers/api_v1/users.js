const router = require('express').Router({ mergeParams: true });
const userController = require('../../controllers/users.js');
const postRouter = require('./posts');

const validate = require('../../utils/validator').validate;
const JoiSchemas = require('../../utils/JoiSchemas');

const mediaParser = require('../../utils/mediaParser');
const guard = require('../../utils/Guard');

const listRouter = require('./lists');

router
  .route('/block')
  .put(guard.allow.notSelf(), userController.block)
  // todo add guard here
  .delete(userController.unblock)

// user's profile
router //
  .route('/')
  .get(userController.getUserProfile)

router //
  .route('/info')
  .get(userController.getUserInfo)
  .put(guard.allow.self(), validate(JoiSchemas.userInfo), userController.updateUserInfo);

router
  .route('/password')
  .put(guard.allow.self(), validate(JoiSchemas.resetPassword), userController.changePassword);

router //
  .route('/avatar')
  .get(userController.getUserAvatar)
  .all(guard.allow.self())
  .put(mediaParser.picture('avatar'), userController.updateUserAvatar)
  .delete(userController.deleteUserAvatar);

router //
  .route('/cover')
  .get(userController.getUserCoverPhoto)
  .all(guard.allow.self())
  .put(mediaParser.picture('cover'), userController.updateUserCoverPhoto)
  .delete(userController.deleteUserCoverPhoto);

// photos only
router //
  .route('/photos')
  .get(userController.getSomeUserPhotos);

// videos only
router //
  .route('/videos')
  .get(userController.getSomeUserVideos);

router.use('/lists', listRouter);

module.exports = router;
