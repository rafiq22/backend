const router = require('express').Router({ mergeParams: true });
const countryController = require('../../controllers/country');

router
    .route('/')
    .get(countryController.getCountryInfo);

module.exports = router;